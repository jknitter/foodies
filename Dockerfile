FROM node:latest

# Set the working directory to /app
WORKDIR /app

RUN npm install

ADD . /app