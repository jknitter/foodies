// const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin')
// const locals = { routes: ['/'] };
const combineLoaders = require('webpack-combine-loaders');
const path = require('path');

module.exports = {
	entry: {
        app: './client/modules/core/App.js'
    },
    output: {
        filename: './client.min.js',
        path: path.resolve(__dirname, 'client')
    },
    module: {
        loaders: [{
            test: /\.js?$/,
            exclude: (/node_modules/),
            use: {
                loader: 'babel-loader?cacheDirectory=true',
                query: {
					presets: ['react', 'es2015'],
					plugins: ["transform-object-rest-spread", 'react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
				}
            }
        },{
        	test: /\.css$/, 
        	use: ['style-loader', 'css-loader']
        }]
    }
}