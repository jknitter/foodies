const express = require('express');
const router = express.Router();
const AuthCtlr = require('../controllers/auth.ctlr');

router.get('/token/:token', AuthCtlr.processToken);
router.post('/login', AuthCtlr.login);
//router.get('/logout', AuthCtlr.logout);
router.post('/signup', AuthCtlr.signup);

module.exports = router;