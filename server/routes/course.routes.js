const express = require('express');
const router = express.Router();
const CourseCtlr = require('../controllers/course.ctlr'),
    checkLogin = require('../controllers/auth/userAuthorization.ctlr').checkLogin;

// add a course to the user's list
router.post('/', checkLogin, CourseCtlr.addCourse);
// edit a course
router.put('/', checkLogin, CourseCtlr.editCourse);

// delete a course
router.delete('/:id', checkLogin, CourseCtlr.deleteCourse);
// return read only data for course
router.get('/:id', CourseCtlr.getCourseById);

module.exports = router;