'use strict';
const express = require('express');
const router = express.Router();

const searchCtlr = require('../controllers/search.ctlr');

router.get('/', searchCtlr.queryMealsByZip);

module.exports = router;