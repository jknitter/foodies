// CRUD data specific to the logged user 
const express = require('express'),
    router = express.Router(),
    CourseCtlr = require('../controllers/course.ctlr'),
    MealCtlr = require('../controllers/meal.ctlr'),
    ProfileCtlr = require('../controllers/user/profile.ctlr'),
    checkLogin = require('../controllers/auth/userAuthorization.ctlr').checkLogin;

// get user info
router.get('/', checkLogin, (req, res) => res.json(req.user));

// get courses created by user
router.get('/courses', checkLogin, CourseCtlr.getCourses);

// get meals created by user
router.get('/meals', checkLogin, MealCtlr.getMeals);


// PROFILE ROUTES //
router.put('/location', checkLogin, ProfileCtlr.saveMyLocationInfo);

router.delete('/avatar', checkLogin, ProfileCtlr.removeAvatar);
router.post('/avatar', checkLogin, ProfileCtlr.saveAvatar);

// EXPORTS //
module.exports = router;