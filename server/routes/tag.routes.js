'use strict';

const express = require('express');
const router = express.Router();

const tagCtlr = require('../controllers/tag.ctlr');

router.get('/:type/:text', tagCtlr.suggestTags);
router.post('/:type/:text', tagCtlr.saveTag);

module.exports = router;