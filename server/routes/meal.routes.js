const express = require('express');
const router = express.Router();
const MealCtlr = require('../controllers/meal.ctlr'),
    checkLogin = require('../controllers/auth/userAuthorization.ctlr').checkLogin;

// add a Meal to the user's list
router.post('/', checkLogin, MealCtlr.addMeal);
// edit a Meal
router.put('/', checkLogin, MealCtlr.editMeal);

// delete a Meal
router.delete('/:id', checkLogin, MealCtlr.deleteMeal);
// get a single meal
router.get('/:id', MealCtlr.getMealById);

module.exports = router;