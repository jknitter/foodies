const express = require('express');
const router = express.Router();
const locCtlr = require('../controllers/location.ctlr');

router.get('/:zip', locCtlr.getLocationByZip);
router.get('/:lat/:lon', locCtlr.getLocationByCoords);

module.exports = router;