const async = require('async'),
    express = require('express'),
    fs = require('fs'),
    
    checkLogin = require('../controllers/auth/userAuthorization.ctlr').checkLogin,
    ImgCtlr = require('../controllers/image.ctlr');
 
const router = express.Router();
// INIT routes //
// return read only data for image
router.get('/:userId/:type/:typeId?/:fileName?', ImgCtlr.getImageByName);


// EXPORTS //
module.exports = router;