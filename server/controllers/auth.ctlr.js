'use strict';

const userAuthenticationController = require('./auth/userAuthentication.ctlr'),
	 userAuthorizationController = require('./auth/userAuthorization.ctlr');
// const userPasswordController = require('./user/userPassword.ctlr'),
// const userProfileController = require('./user/userProfile.ctlr'); -- change to recover , reset

module.exports = Object.assign({},
	userAuthenticationController,
	userAuthorizationController/*,
	userPasswordController,
	userProfileController*/
);