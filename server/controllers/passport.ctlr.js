const glob = require('glob'),
	passport = require('passport'),
	path = require('path'),

	mongoose = require('mongoose'),
	User = mongoose.model('User');

// Serialize sessions
passport.serializeUser(function(user, cb) {
	cb(null, user.id);
});

// Deserialize sessions
passport.deserializeUser(function(id, cb) {
	User.findOne({ _id: id }, '-salt -password', function(err, user) { cb(err, user); });
});

// Initialize strategies
glob('server/controllers/strategies/*.js', function(err, strategies){
	strategies.forEach( s => require( path.resolve(s)) );
});