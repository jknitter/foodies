'use strict';

const async = require('async'),
	nodemailer = require('nodemailer'),
	config = require('../config');

function sendVerifySignupToken(req, res, token, done){
	var user = req.body;
	var htmlConfig = {
		userName: req.body.userName,
		url: 'http://' + req.headers.host + '/verifySignup/' + token
	}

	res.render('email/verifySignup', htmlConfig, (err, emailHTML) => {
		if(err){
			done(err);
			return;
		}

		var smtpTransport = nodemailer.createTransport(config.mailer.options);
		var mailOptions = {
			to: user.email,
			from: config.mailer.from,
			subject: 'Verify Email Address',
			html: emailHTML
		};

		smtpTransport.sendMail(mailOptions, done);
	});
};

// EXPORTS //
module.exports.sendVerifySignupToken = sendVerifySignupToken;