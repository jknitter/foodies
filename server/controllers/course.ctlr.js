const async = require('async'),
	mongoose = require('mongoose'),

	dataCtlr = require('../../data/controllers/data.ctlr'),
	ImgCtlr = require('../controllers/image.ctlr'),
	{validateCourse} = require('../../shared/validate'),
	CourseModel = mongoose.model('Course'),
	UserModel = mongoose.model('User');
	
function addCourse(req, res){
	const posted = req.body.course;
	const { isValid, errors } = validateCourse(posted); 
	
	if(!isValid)
		return res.status(400).send(errors);

	posted.cookId = req.user._id;
	// save new course
	const course = new CourseModel(posted);

	async.waterfall([
		course.save.bind(course),
		(savedCourse,wtf,cb) => {
			UserModel.findOneAndUpdate(
				{ _id: req.user._id }, 
				{ $addToSet: { courses: savedCourse._id } },
				() => cb(null, savedCourse)
			)
		},
		(savedCourse, cb) => {
			if(req.body.addedPics){
				const path = `${savedCourse.cookId.toString()}/courses/${savedCourse._id}`;
				ImgCtlr.upload(path, req.body.addedPics)
					.then(() => cb(null, savedCourse))
					.catch(err => cb(err));	
			}
			else {
				cb(null,savedCourse);
			}	
		}
	], (err, results) => {
		if(err) res.status(400).json({error: err});
		else res.status(200).json(results);
	});
}	

function deleteCourse(req, res){
	const courseId = req.params.id;
	
	async.waterfall([
		(cb) => {
			CourseModel.findOneAndRemove({ _id: courseId })
				.exec()
				.then(course => cb(null, course.toJSON()))
				.catch(cb)
		},
		(deletedCourse, cb) => {
			UserModel.findByIdAndUpdate(
				{_id: deletedCourse.cookId}, 
				{$pull:{courses: courseId}},
				{new: true}
			)
			.exec()
			.then(user => cb(null, deletedCourse))
			.catch(cb)
		},
		(deletedCourse, cb) => {
			const {cookId, _id} = deletedCourse; 
			ImgCtlr.deleteImages(`${cookId}/courses/${_id}`)
				.then(() => cb(null, deletedCourse))
				.catch(err => {
					// we dont care if the file is not there, we were deleting it
					if(err.code === 'ENOENT') 
						cb(null, deletedCourse)
					else
						cb(err);
				});
		}
	],
		(err, results) => err ? res.status(400).json({error: err}) : res.sendStatus(200)
	);	
}

// function deleteCoursePics(req, res){
// 	const files = req.body;

// 	CourseModel.findByIdAndUpdate(
// 		{ _id: req.params.courseId },
// 		{ $pull: { pics: { $in: files } } },
// 		{ new: true }
// 	)
// 	.exec()
// 	.then(() => res.sendStatus(200))
// 	.catch(err2 => res.status(400).json(err2))
// }

function editCourse(req, res){
	const course = req.body.course;
	const { isValid, errors } = validateCourse(course);
	
	if(!isValid)
		return res.status(400).send(errors);
	
	// collect promises, first, update course
	let fns = [CourseModel.findOneAndUpdate({ _id: course._id }, course).exec()];
	// check removed pics
	const { removed, addedPics } = req.body;
	const path = `${course.cookId.toString()}/courses/${course._id}`;
	
	if(removed && removed.length > 0)
		fns.push(ImgCtlr.deleteImages(path, removed));

	if(addedPics && addedPics.length > 0)
		fns.push(ImgCtlr.upload(path, addedPics));
	
	Promise.all(fns)
		.then(() => res.json(course))
		.catch(err => res.status(500).json({ error: err }));
}

function getCourseById(req, res){
	const course = dataCtlr.getData('courses', req.params.id);
	if(!course)
		res.status(500).json({error: 'Course Not Found'});
	else
		res.json(course);
}

function getCourses(req, res){
	CourseModel.find({cookId: req.user._id}).exec((err, results) => {
		if(err) res.status(400).json({ error: err });
		else res.json(results);
	});
};


// EXPORTS //
module.exports.addCourse = addCourse;
module.exports.deleteCourse = deleteCourse;
module.exports.editCourse = editCourse;
module.exports.getCourseById = getCourseById;
module.exports.getCourses = getCourses;
