const dataCtlr = require('../../data/controllers/data.ctlr'),
	{ validateMeal } = require('../../shared/validate'),
	mongoose = require('mongoose'),
	MealModel = mongoose.model('Meal');

function addMeal(req, res){
	const posted = req.body;
	const { isValid, errors } = validateMeal(posted);

	if (!isValid)
		return res.status(400).send(errors);

	posted.userId = req.user.userId;

	const meal = new MealModel(posted);
	meal.save((err, resp) => err ? 
		res.status(400).json({ error: err }) : 
		res.json(resp));
};

function deleteMeal(req, res){
	MealModel.findOneAndRemove(req.params.id, (err, resp) => {
		err ?
			res.status(400).json({ error: err }) :
			res.json(resp)
	});
};

function editMeal(req, res){
	const meal = req.body;
	const { isValid, errors } = validateMeal(meal); 
	
	if(!isValid)
		return res.status(400).send(errors);
	
	MealModel.findOneAndUpdate({_id: meal._id}, meal).exec()
		// return what was sent in
		.then(resp => res.json(meal))
		.catch(err => res.status(400).json({error: err}));
};

function getMealById(req, res) {
	const mealId = req.params.id;
	//TODO: use hotmeals for now, later "meals" and "hotmeals" will be different
	// where hotmeals will be (semi) local and made to order or ready soon (allow user to select time period)
	const meal = dataCtlr.getData('hotmeals', mealId); //, 'mealId');
	if (!meal)
		res.status(400).json({ error: 'Meal not Found' });
	else
		res.json(meal);
};

function getMeals(req, res){
	const user = req.user;
	if (!req.user)
		res.status(403).json({ error: 'Not Auth to get Meals' });

	MealModel.find({ userId: user.userId }).exec((err, results) => {
		if (err) res.status(400).json({ error: err });
		else res.json(results);
	});	
};

// EXPORTS //
module.exports.addMeal = addMeal;
module.exports.deleteMeal = deleteMeal;
module.exports.editMeal = editMeal;
module.exports.getMealById = getMealById;
module.exports.getMeals = getMeals;