'use strict';

const mongoose = require('mongoose'),
	User = mongoose.model('User'),

	passport = require('passport'),
	passportLocal = require('passport-local'),
	LocalStrategy = passportLocal.Strategy;
	
// Use local strategy
passport.use( 
	new LocalStrategy( 
		{usernameField: 'userId', passwordField: 'pwd1' }, 
		(userId, password, cb) => {
			User.findOne({ userId: userId })
			.populate('courses')
			.exec((err, user) => {
				if (err)  
					return cb(err);

				if (!user || !user.authenticate(password))
					return cb(null, false, { message: 'Invalid Login' });

				return cb(null, user);
			});
		}
	)
);