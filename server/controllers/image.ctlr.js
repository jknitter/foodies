'use strict';

const async = require('async'),
    fs = require('fs'),
    path = require('path'),
    {promisify} = require('util'),
    rimraf = require('rimraf');
    
const basePath = 'data/uploads';

function ensurePath(path){
    const parts = path.split('/');
    let dest = basePath;
    let fns = []
    for(let i = 0; i<parts.length; i++){
        const curPath = dest += '/'+parts[i]; 
        fns.push(ensureDir(curPath));
    };
    
    return Promise.all(fns);
}

function ensureDir(dirPath){
    return new Promise((resolve, reject) => {
        const checkPath = path.resolve(dirPath);    
        promisify(fs.access)(checkPath)
            .then(() => resolve(dirPath))
            .catch (err => {
                if(err && err.code === 'ENOENT') {
                    promisify(fs.mkdir)(checkPath)
                        .then(() => resolve(dirPath))
                        .catch(err2 => reject(err2));
                };
            });
    });
};

function deleteImages(path, names=[]){
    if(names.length === 0)
        return promisify(rimraf)(`${basePath}/${path}`);

    return Promise.all(
        names.map(name => promisify(fs.unlink)(`${basePath}/${path}/${name}`))
    );
}

function getUrl(params){
    const { userId, type, typeId, fileName } = params;
    let url = `${basePath}/${userId}/${type}`;
    if(typeId) url += typeId;
    if(fileName) url += fileName;

    return url;
}

function getImageByName(req, res){
    const url = path.resolve( getUrl(req.params) )
    res.sendFile(url);
}

function upload(path, files){
    const write = promisify(fs.writeFile);
    return ensurePath(path).then(() => {
        const fns = files.map(file => {
            const src = file.src.replace(/^data:image\/(png|gif|jpeg);base64,/, '');
            return write( `${basePath}/${path}/${file.name}`, new Buffer(src, 'base64') );
        });
        return Promise.all(fns);
    });
}

// EXPORTS //
module.exports.deleteImages = deleteImages;
module.exports.getImageByName = getImageByName;
module.exports.upload = upload;