'use strict';

const mongoose = require('mongoose'),
    UserModel = mongoose.model('User'),
    ImgCtlr = require('../../controllers/image.ctlr'),

    { validateProfileLocation } = require('../../../shared/validate');

function removeAvatar(req, res){
    const userId = req.user._id;
    ImgCtlr.deleteImages(userId, req.user.avatar)
        .then(UserModel.findByIdAndUpdate(
            userId,
            {avatar: false},
            {new: true}
        ).exec())
        .then(() => res.sendStatus(200))
        .catch(err => res.status(500).json(err));
}

function saveAvatar(req, res){
    const fn = req.body.name;
    // includes dot .
    const ext = fn.substr(fn.lastIndexOf('.'));
    const name = 'avatar' + ext;

    ImgCtlr.upload(`${req.user._id}`, [{name: name, src: req.body.src}])
        .then(UserModel.findByIdAndUpdate(req.user._id, {avatar: name}, {new:true}).exec())
        .then(user => res.status(200).json(user))
        .catch(err => res.sendStatus(500).json(err));
}

function saveMyLocationInfo(req, res){
    const loc = req.body;
    const {isValid, errors} = validateProfileLocation(loc);
    if(!isValid)
        res.status(400).json({errors});
    else
        UserModel.findByIdAndUpdate(req.user._id, {location:loc}, {new:true})
            .exec()
            .then(user => res.json(user.location))
            .catch(err => res.staus(500).json({errors: { loc: err }}) );
}

module.exports.removeAvatar = removeAvatar;
module.exports.saveAvatar = saveAvatar;
module.exports.saveMyLocationInfo = saveMyLocationInfo;