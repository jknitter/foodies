const axios = require('axios');

const dataCtlr = require('../../data/controllers/data.ctlr');

function getLocationByCoords(req, res){ 
	const { lat, lon } = req.params;
	const url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + '%2C' + lon + '&language=en';
	return axios.get(url).then(resp => {
		let json = resp.data.results;
		if(!json) 
			return res.status(400).json({error: 'No Results'});

		json = json.find(r => r.types.find(t => t === 'postal_code'));
		if(!json)
			return res.status(400).json({error: 'postal_code not found'});

		json = json.address_components;
		let location = {
			zipcode: json.find(ac => ac.types.indexOf('postal_code') > -1).long_name,
			placename: json.find(ac => ac.types.indexOf('locality') > -1).long_name,
			county: json.find(ac => ac.types.indexOf('administrative_area_level_2') > -1).long_name,
			state: json.find(ac => ac.types.indexOf('administrative_area_level_1') > -1).short_name,
			statename: json.find(ac => ac.types.indexOf('administrative_area_level_1') > -1).long_name,
			country: json.find(ac => ac.types.indexOf('country') > -1).short_name,
			coords: { lat: lat, lon: lon }
		};	
	
		res.json(location);
	})
	.catch(err => res.status(400).json({error: err}));
};

function getLocationByZip(req, res){
	let zip = "";
	// internal server call, which means req is zip
	if(!res) zip = req;
	// browser request
	else zip = req.params.zip;
	
	// get coords of zipcode
	const location = dataCtlr.getData('zipcoords', zip);
	if(res)	return res.json(location);
	else return location;
};

// EXPORTS //
module.exports.getLocationByCoords = getLocationByCoords;
module.exports.getLocationByZip = getLocationByZip;
