'use strict';

const dataCtlr = require('../../data/controllers/data.ctlr');
const locCtlr = require('./location.ctlr');
const locUtil = require('../../shared/location.util');
const oneMileInDegrees = .185;

const getSeachBox = function(lat1, lon1, range, unit){
	try {
		const delta = range * oneMileInDegrees;
		const lat = parseFloat(lat1);
		const lon = parseFloat(lon1); 
		
		return { 
			center: {lat: lat , lon: lon },
			tl: {lat: lat-delta, lon: lon-delta},
			br: {lat: lat+delta, lon: lon+delta},
			isInBox: function(checkLat, checkLon) {
				return (
					checkLat > this.tl.lat && checkLat < this.br.lat &&
					checkLon > this.tl.lon && checkLon < this.br.lon 
				);
			},
			isInRadius: function(checkLat, checkLon){
				return (locUtil.getDistanceFromLatLons(checkLat, checkLon, lat, lon) <= range);
			},
			allInBox: function(objsWithCoords, idProp){
				if(!idProp) console.error('allInBox, idProp is undefined')
				let thoseInside = {};
				for(let key in objsWithCoords){
					const owc = objsWithCoords[key];
					if(this.isInBox(owc.coords.lat, owc.coords.lon)){
						thoseInside[owc[idProp]] = owc;
					}
				};

				return thoseInside;
			},
			allInCircle: function(objsWithCoords, idProp){
				// reduce the result set before hitting the heavy calc
				objsWithCoords = this.allInBox(objsWithCoords, idProp);

				let thoseInside = {};
				for(let key in objsWithCoords){
					const owc = objsWithCoords[key];
					if(this.isInRadius(owc.coords.lat, owc.coords.lon)){
						thoseInside[owc[idProp]] = owc;
					}
				};

				return thoseInside;
			}
		}
	} 
	catch(err){
		console.log('getSeachBox.err', err)
	}
};

function queryMealsByZip(req, res){
	// get list 
	const zip = req.query.zip;
	const loc = locCtlr.getLocationByZip(zip);
	if(!loc) 
		return res.status(400).json({errors: {zip: 'Invalid Zipcode'}});

	const meals = searchMealsByCoords(loc.coords.lat, loc.coords.lon, 5);
	return res.json( meals );
};

// function queryMealsByCoords(req, res){
// 	const {lat, lon, range} = req.params;
// 	const meals = searchMealsByCoords(lat, lon, range || 5);
// 	return res.json( meals );
// };

function searchMealsByCoords(lat, lon, range){
	const allMeals = dataCtlr.getData('hotmeals');
	return getSeachBox(lat, lon, range).allInCircle(allMeals, 'mealId');	
};


// EXPORTS //
module.exports.getSeachBox = getSeachBox;
module.exports.queryMealsByZip = queryMealsByZip;