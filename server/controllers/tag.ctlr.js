'use strict';

const async = require('async'),
    config = require('../config'),
    mongoose = require('mongoose'),
    TagModel = mongoose.model('Tag');

function suggestTags(req, res){
    const { type, text } = req.params;
    TagModel.find({
        type: type,
        text: { '$regex': text, '$options': 'i' }
    })
    .select('text')
    .limit(5)
    .exec( (err, list=[]) => {
        if(err) console.log(err);
        res.json(list);
    });
};

function saveTag(req, res){
    const tag = new TagModel(req.params);
    tag.save(err => {
        if(err) console.log(err);
        res.sendStatus(200);
    });
}

// EXPORTS //
module.exports.suggestTags = suggestTags;
module.exports.saveTag = saveTag;