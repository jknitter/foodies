'use strict';

const async = require('async'),
	passport = require('passport'),
	jwt = require('jsonwebtoken'),
  	mongoose = require('mongoose'),
	UserModel = mongoose.model('User'),
    TokenModel = mongoose.model('Token'),

  	dataCtlr = require('../../../data/controllers/data.ctlr'),
  	emailCtlr = require('../email.ctlr'),
 	{validateSignup} = require('../../../shared/validate'),
	config = require('../../config');

function createVerifySignupToken(user){	
	return function(cb){
		var token = new TokenModel();
		token.userId = user._id;
		token.type = 'signup';

		return token.save( (err, respToken) => cb(err, respToken));
	}
};

function login(req, res, next) {
	passport.authenticate('local', function (err, user, info) {
		if (err || !user)
			return res.status(400).json({ errors: { err: info.message } });
		else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;
		
			const token = jwt.sign(
				{_id: user._id, userId: user.userId},
				config.db.sessionSecret,
				{ expiresIn: "2h" }
			);

			res.json({ token });
		}
	})(req, res, next);
};

function processToken(req, res){
	const token = req.params.token;

	// look for the token in the db
	TokenModel.findOne({token: token}).exec(function(verifySignupFindErr, verifySignupRecord) {
		if(verifySignupFindErr) 
			return res.send(verifySignupFindErr);
		
		// record not found
		if(!verifySignupRecord) 
			return res.status(400).json({error: 'Failed to find that verification token'});

		// record found, is token still valid
		if(verifySignupRecord.expires < Date.now()) 
			return res.status(400).json({error:'The verification token has expired'});
		// find the user in the db
		UserModel.findOne({userId: verifySignupRecord.userId}).exec(function(userFindErr, user){
			// Remove sensitive data before login
			delete user._doc.password;
			delete user._doc.salt;
			
			if(userFindErr) 
				return res.status(400).json({ error: userFindErr });
			
			// record not found
			if(!user)
				// this shouldnt happen, we created a verification token for a user that didnt sign up? 
				return res.status(400).json({ error: 'Failed to find the user linked to that token' });

			// this shouldnt happen, user would hit "token not found" since after 
			// a user is verified the record for the token is deleted
			if(user.role !== 'pending'){
				return res.status(400).json({error: 'User linked to token is already verified'});
			}
			else {
				// set the role
				user.role = 'user';
				//user.role = 'admin';
			
				user.save(function(err){
					if(err)
						return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation(err)) );
					
					// delete the record in the collection, now that the user is verified
					verifySignupRecord.remove();

					return res.status(200).json({success: true});
				});
			}					
		});
	});
};

function signup(req, res) {
	let user = req.body.user;
	const { isValid, errors } = validateSignup(user);

	if (!isValid)
		return res.status(400).send(errors);

	req.body.user.password = user.pwd1;

	const loc = dataCtlr.getData('zipcoords', user.zip);
	user.location = loc;

	delete loc.lat;
	delete loc.lon;
	delete user.zip;
	delete user.pwd1;
	delete user.pwd2;
	new UserModel(user).save(err => {
		if(err){
			// mongodb code unique key violation
			if (err.code === 11000 || err.code === 11001)
				return res.status(400).json({ userId: "UserId is unavaliable" });
			else
				return res.status(400).json(err);
		}
		else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			async.waterfall([
				// create verification token
				createVerifySignupToken(user),
				// 	// send user an email 
				// 	emailCtlr.sendVerifySignupToken.bind(emailCtlr, req, res)
			], (err) => {
				if (err)
					return res.status(400).json({ error: err });

				return res.json({ success: true, msg: "Thanks we got it, check ur email to verify your account" });
			});
		}
	});
};


// EXPORTS //
module.exports.signup = signup;
module.exports.login = login;
module.exports.processToken = processToken;

// module.exports.logout = function(req, res) {
// 	req.logout();
// 	res.redirect('/');
// };

/*		// resendVerifySignupToken: function(req, res){
		// 	if(!req.body.userName){
		// 		return res.status(400).json(responseCtlr.createErrorResponse(errorsCtlr.getValidation('No user name sent', 'resend')) );
		// 	}

		// 	// find the user in the db
		// 	UserModel.findOne({userName: req.body.userName}).exec(function(userFindErr, user){
		// 		if(userFindErr) 
		// 			return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation(userFindErr, 'resend')) );
		// 		// record not found
		// 		if(!user)
		// 			// this shouldnt happen, we created a verification token for a user that didnt get signed up? 
		// 			return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation('Failed to find that user', 'resend')) );

		// 		// Remove sensitive data before login
		// 		delete user._doc.password;
		// 		delete user._doc.salt;

		// 		req.body.email = user.email;

		// 		async.waterfall([
		// 			// create verification record
		// 			createVerifySignupToken.bind(this, req, res),
		// 			// send user an email 
		// 			emailCtlr.sendVerifySignupToken.bind(emailCtlr, req, res)
		// 		], function(err){
		// 			if(err){
		// 				return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation(err, 'resend')) );
		// 			}	

		// 			return res.status(200).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation('Email Sent!', 'resend', 'success')) );
		// 		});
		// 	});
		// },

		//////}
		/*,

		oauthCallback: function(strategy) {
			return function(req, res, next) {
				passport.authenticate(strategy, function(err, user, redirectURL) {
					if (err || !user) {
						return res.redirect('/#!/signin');
					}
					req.login(user, function(err) {
						if (err) {
							return res.redirect('/#!/signin');
						}

						return res.redirect(redirectURL || '/');
					});
				})(req, res, next);
			};
		},

		// Helper function to save or update a OAuth user profile
		saveOAuthUserProfile: function(req, providerUserProfile, done) {
			if (!req.user) {
				// Define a search query fields
				var searchMainProviderIdentifierField = 'providerData.' + providerUserProfile.providerIdentifierField;
				var searchAdditionalProviderIdentifierField = 'additionalProvidersData.' + providerUserProfile.provider + '.' + providerUserProfile.providerIdentifierField;

				// Define main provider search query
				var mainProviderSearchQuery = {};
				mainProviderSearchQuery.provider = providerUserProfile.provider;
				mainProviderSearchQuery[searchMainProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

				// Define additional provider search query
				var additionalProviderSearchQuery = {};
				additionalProviderSearchQuery[searchAdditionalProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

				// Define a search query to find existing user with current provider profile
				var searchQuery = {
					$or: [mainProviderSearchQuery, additionalProviderSearchQuery]
				};

				User.findOne(searchQuery, function(err, user) {
					if (err) {
						return done(err);
					} else {
						if (!user) {
							var possibleUsername = providerUserProfile.userName || ((providerUserProfile.email) ? providerUserProfile.email.split('@')[0] : '');

							User.findUniqueUsername(possibleUsername, null, function(availableUsername) {
								user = new User({
									firstName: providerUserProfile.firstName,
									lastName: providerUserProfile.lastName,
									userName: availableUsername,
									name: providerUserProfile.name,
									email: providerUserProfile.email,
									provider: providerUserProfile.provider,
									providerData: providerUserProfile.providerData
								});

								// And save the user
								user.save(function(err) {
									return done(err, user);
								});
							});
						} else {
							return done(err, user);
						}
					}
				});
			} else {
				// User is already logged in, join the provider data to the existing user
				var user = req.user;

				// Check if user exists, is not signed in using this provider, and doesn't have that provider data already configured
				if (user.provider !== providerUserProfile.provider && (!user.additionalProvidersData || !user.additionalProvidersData[providerUserProfile.provider])) {
					// Add the provider data to the additional provider data field
					if (!user.additionalProvidersData) user.additionalProvidersData = {};
					user.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;

					// Then tell mongoose that we've updated the additionalProvidersData field
					user.markModified('additionalProvidersData');

					// And save the user
					user.save(function(err) {
						return done(err, user, '/#!/settings/accounts');
					});
				} else {
					return done(new Error('User is already connected using this provider'), user);
				}
			}
		},

		removeOAuthProvider: function(req, res, next) {
			var user = req.user;
			var provider = req.param('provider');

			if (user && provider) {
				// Delete the additional provider
				if (user.additionalProvidersData[provider]) {
					delete user.additionalProvidersData[provider];

					// Then tell mongoose that we've updated the additionalProvidersData field
					user.markModified('additionalProvidersData');
				}

				user.save(function(err) {
					if (err) {
						return res.status(400).json( errorsCtlr.getValidation(err) );
					} else {
						req.login(user, function(err) {
							if (err) {
								res.status(400).json( errorsCtlr.getValidation(err) );
							} else {
								res.jsonp(user);
							}
						});
					}
				});
			}
		}
		
	}
});*/