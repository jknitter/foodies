'use strict';

const mongoose = require('mongoose'),
	UserModel = mongoose.model('User'),

	jwt = require('jsonwebtoken'),
	config = require('../../config');

function checkLogin(req, res, next){
	const header = req.header('Authorization');
	let token;

	if(header)
		token = header.split(' ')[1];
	
	if(!token)
		return res.status(401).json({error: 'Not Auth'});
	
	jwt.verify(token, config.db.sessionSecret, (err, decoded) => {
		if(err)
			return res.status(401).json({error: 'Failed to verify token:', err});

		UserModel.findOne({ userId: decoded.userId })
			.populate('courses') 
			.exec((err2, user) => {
				if(err2)  
					return res.status(401).json({error: err2});
				if(!user)// || !user.authenticate(password))
					return res.status(401).json({error: 'Invalid Token, user not found'});
				
				req.user = user;
				next();
			});	
	});
};

// EXPORTS //
module.exports.checkLogin = checkLogin;