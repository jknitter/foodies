define(function(){
	var ip = 'www.xyz.com';
	var port = 4001;

 	return { 
 		port: port,
		css: [ 
			'/css/bootstrap.min.css',
			'/css/font-awesome.min.css',
			'/css/app.css'
		  ], 
		  js: ['/client.min.js'],
		  db: {
			  //url: 'mongodb://localhost:27017/', 
			  url: 'mongo:27017/', // for docker
			  name: 'dev',
			  sessionSecret: 'cat poop',
			  sessionCollection: 'user_session'
		  },
	}
});