'use strict';

module.exports = {
    port: 4001,
    css: [
        '/css/bootstrap.min.css',
        '/css/font-awesome.min.css',
        
        '/css/me-courses-page.css',
        '/css/course-card-component.css',

        '/css/me-meals-page.css',

        '/css/me-prefs-page.css',
        
        '/css/profile-page.css',
        
        '/css/search-page.css',
        
        '/css/overlay-component.css',
        '/css/tag-component.css',
        
        '/css/app.css'
        //"https://fonts.googleapis.com/css?family=Alfa+Slab+One|Zilla+Slab"
    ],
    js: ['/client.min.js'],
    db: {
        url: 'mongodb://localhost:27017/',
        //url: 'mongo:27017/', //docker
        name: 'dev',
        sessionSecret: 'cat poop',
        sessionCollection: 'user_session'
    },
    mailer: {
        from: process.env.MAILER_FROM || 'loanstar.donnotreply@outlook.com',
        options: {
            host: 'smtp-mail.outlook.com',
            service: process.env.MAILER_SERVICE_PROVIDER || 'outlook',
            auth: {
                user: process.env.MAILER_EMAIL_ID || 'loanstar.donotreply@outlook.com',
                pass: process.env.MAILER_PASSWORD || 'klSAnmXZ9021'
            }
        }
    }
};