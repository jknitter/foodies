'use strict';

const env = "./" + (process.env.NODE_ENV || 'dev');
const config = require( env );

module.exports = Object.assign({
	title: 'Food Matters', 
	description: '',
	keywords: '',
	templateEngine: 'swig'
}, config);