'use strict';

const { createServer } = require('http'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	consolidate = require('consolidate'),
	express = require('express'),
	session = require('express-session'),
	glob = require('glob');

// intall mongodb models
glob.sync('data/models/*.model.js').forEach( path => require('./' + path) );

const mongoose = require('mongoose'),
	connectMongo = require('connect-mongo'),
	passport = require('passport'),

	config = require('./server/config/index');


// CONFIG SERVER //
let app = express();
const server = createServer(app);

// Assign config for server views to use
Object.assign(app.locals, config);

// server side templating 
app.engine('html', consolidate[config.templateEngine]);
app.set('view engine', 'html');
app.set('views','./server/views');	

// static folders
app.use('/', express.static('./client'));

// middleware
app.use(bodyParser.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// passport middleware
app.use(passport.initialize());
app.use(passport.session());
// passport setup needs User model, so this comes after model loading
require('./server/controllers/passport.ctlr');

// intall routes
glob.sync('server/routes/*.routes.js').forEach(path => {
	const router = require('./' + path);
	const lastSlash = path.lastIndexOf('/');
	const firstDot = path.indexOf('.');
	const module = path.slice(lastSlash+1, firstDot);
	app.use('/api/' + module, router);
});

// this should always be the last route for a single page app.
app.route('/*').get((req, res) => {
	res.render('index');
});


// init loading of data from files
const dc = require('./data/controllers/data.ctlr');

// // Connect using MongoClient
const { sessionCollection, sessionSecret, url, name } = config.db;

// Setup mongo session, connection	
//const mongoStore = connectMongo(session);
const db = mongoose.connect(url + name).then((err, dbInistance) => {
	if (err)
		 return console.log("MongoDb is not connected", err); 
	
	console.log("MongoDb is connected!");
	
	// app.use(session({
	// 	saveUninitialized: true,
	// 	resave: false,
	// 	secret: sessionSecret,
	// 	store: new mongoStore({
	// 		db: db.connection.db,
	// 		collection: sessionCollection,
	// 		ttl: 6*3600 // 6 hr session
	// 	})
	// }));
	
	// RUN SERVER //
	server.listen(config.port, () => { 
		console.log('Food is served on port %s', config.port);
	});	
}).
catch(err => console.log(err));


