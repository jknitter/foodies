import axios from 'axios';
import React from 'react';
import {connect} from 'react-redux';
import { hotMealsFetched, clearMeals } from '../actions/meals';
import MealList from '../meals/MealList';

import history from '../core/history';
import Enums from '../core/Enums';
import utils from '../../../shared/location.util';

@connect(store => {
	return { 
		user: store.user.user,
		meal: store.meals.meal
	}
})
export default class HotMealsPanel extends React.Component {
	constructor(props){
		super(props);
	}

	render(){
		return <div class="meal-panel">
			<ul>
				<li onClick={() => history.push('/me/account')}>
					<i class="fa fa-user-circle" />Accounting
				</li>
				<li onClick={() => history.push('/me/prefs')}>
					<i class="fa fa-gears" />Settings
				</li>
			</ul>
		</div>
	}
}