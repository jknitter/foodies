import React from 'react';
import history from '../core/history';
import moment from 'moment';

import { hotMealSelected } from '../actions/meals';
import { HotMeals as Enum } from '../core/Enums';
import utils from '../../../shared/location.util';

class MealListItem extends React.Component {
	constructor(props) {
		super(props);
	}

	render(){
		let title = '';
		this.props.courses.forEach((course, idx) => {
			title += course.name + (idx < this.props.courses.length-1 ? ', ' : '');
		});

		const { selected, uCoords, mealId } = this.props;
		let time = this.props.time;
		// calc the string to display baased on the meal's scheduled time
		time = typeof(time) === 'string' ? time : moment(time).fromNow();

		const elClass = 'meal ' + (this.props.selected && this.props.selected.mealId === mealId ? 'active' : '');
		
		return (
			<div className={elClass} onClick={() => {
				history.push('/meal/' + this.props.mealId);
				this.props.dispatch(hotMealSelected(this.props.mealId));
			}} >	
				<div>{title}</div>
				<div>Ready {time}</div>
				<div>
					<strong>{this.props.cookName}</strong> is <strong>{this.props.dist}</strong> miles away 
				</div>
			</div>
		)
	}
}

export default class MealList extends React.Component {
	constructor(props){	super(props); } 
	
	render(){
		const uCoords = this.props.loc.coords;
		
		let meals = Object.values(this.props.meals);
		meals = meals.length > 0 ?
			meals.map( (hm, i) => {
				// calc the string to display distance
				const { lat, lon } = hm.coords;
				const dist = utils.getDistanceFromLatLons(lat, lon, uCoords.lat, uCoords.lon);		

				return <MealListItem
					dist={dist}
					dispatch={this.props.dispatch} 
					selected={this.props.selected} 
					key={i} 
					{...hm} /> 
			}) 
			: ""
		
		return <div class='meals-list'>
			{meals}
		</div>
	}
}
