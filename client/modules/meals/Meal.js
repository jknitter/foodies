import React from 'react';
import axios from 'axios';

import CourseView from '../me/courses/CourseView';

import history from '../core/history';
import LabeledDrawer from '../components/LabeledDrawer'

export default class Meal extends React.Component {
	constructor(props){
		super(props);
	} 

	componentWillMount(){
		if(this.props.mealId)
			this.setState({...this.props.mealId})
		else {
			this.setState({ courses: [], cookName:'' });
		
			// if no data we need to fetch it
			axios.get('/api/meal/'+ this.props.match.params.mealId)
			.then(resp => {
				this.setState({...resp.data});
			});
		}
	}

	render(){
		const courses = this.state.courses.map((c, i) => {
			return <CourseView {...c}/>
		});

		return <div class='meal-container'>
			{this.state.cookName}
			{courses}
		</div>
	}
}
