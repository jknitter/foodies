import Enums from '../core/Enums';
const Enum = Enums.User;

const initialState = { 
	isAuth: false,
	user: {
		location: {},
		courses: [],
		meals: []
	}
};

export default function(state={...initialState}, action){
	switch(action.type){
		case Enum.event.login: { 
			const user = Object.assign({}, state.user, action.user );
			return { ...state, user: user, isAuth: !!action.user.userId };

		}
		case Enum.event.logout: { 
			return initialState;
		}
		case Enum.event.locationFetched: {
			// check user's saved location data
			// check saved data is different than incoming
			// append valid data to the user object
			const loc = Object.assign({}, state.user.location, action.payload );
			const user = Object.assign({}, state.user, {location: loc} );
			
			return { ...state, user };	
		}


		// COURSES //
		case Enums.Me.courseAdded: {
			const courses = state.user.courses.slice();
			courses.push(action.course);

			const user = Object.assign({}, state.user, {courses: courses} );
			return { ...state, user };
		}

		case Enums.Me.courseEdited: {
			const courses = state.user.courses.slice();
			const editedIdx = courses.findIndex((c,i,a)=> c._id === action.course._id)
			courses.splice(editedIdx, 1, action.course)
			const user = Object.assign({}, state.user, {courses: courses} );
			return { ...state, user };
		}

		case Enums.Me.courseRemoved: {
			const index = state.user.courses.indexOf(action.course);
			let courses = state.user.courses.slice();
			courses.splice(index, 1);

			const user = Object.assign({}, state.user, {courses: courses} );
			return { ...state, user };
		}

		case Enums.Me.coursesFetched: {
			const user = Object.assign({}, state.user, {courses: action.courses});
			return { ...state, user };
		}


		// meals
		case Enums.Me.mealAdded: {
			const meals = state.user.meals.slice();
			meals.push(action.meal);

			const user = Object.assign({}, state.user, { meals: meals });
			return { ...state, user };
		}

		case Enums.Me.mealRemoved: {
			const index = state.user.meals.indexOf(action.meal);
			let meals = state.user.meals.slice();
			meals.splice(index, 1);

			const user = Object.assign({}, state.user, { meals: meals });
			return { ...state, user };
		}

		case Enums.Me.mealsFetched: {
			const user = Object.assign({}, state.user, { meals: action.meals });
			return { ...state, user };
		}

		default: 
			return state;
	}
}