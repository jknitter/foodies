import Enums from '../core/Enums';

const initState = {	meal: null, meals: {} };

export default function(state=initState, action){
	switch(action.type){
		case Enums.HotMeals.clear: { 
			return {...state, meals: {}, meal: null};
		}
		case Enums.HotMeals.fetched: { 
			return {...state, meals: action.payload, meal: null};
		}
		case Enums.HotMeals.selected: { 
			return {...state, meal: state.meals[action.payload]};
		}
		default: 
			return state;
	}
}