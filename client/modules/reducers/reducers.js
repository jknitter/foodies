import { combineReducers } from 'redux';

import header from './header';
import meals from './meals';
import user from './user';

export default combineReducers({
	header, meals, user
});
