import Enums from '../core/Enums';

const initState = { showMenu: false };

export default function(state=initState, action){
	switch(action.type){
		case Enums.Header.menuClick: { 
			return {...state, showMenu: !state.showMenu};
		}
		default: 
			return state;
	}
}