import React from 'react';
import {HotMeals as Enum} from '../core/Enums';

export function clearMeals(){
	return {type: Enum.clear}; 
}

export function hotMealsFetched(meals){
	return {type: Enum.fetched,	payload: meals};	
}

export function hotMealSelected(mealId){
	return {type: Enum.selected, payload: mealId}; 
}
