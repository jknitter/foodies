import React from 'react';
import axios from 'axios';
import jwt from 'jsonwebtoken';

import Enums from '../core/Enums';

const setAuthorizationHeader = function(token){
	if(token)
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
	else
		delete axios.defaults.headers.common['Authorization'];
}

export function login(token) {
	return dispatch => {
		// put validation token in localstorage
		localStorage.setItem('jwtToken', token);
		// tell axios to send the token in the request header
		setAuthorizationHeader(token);
		return setCurrentUser(dispatch);
	}
};

export function logout() {
	return dispatch => {
		// remove validation token from localstorage
		localStorage.removeItem('jwtToken');
		// remove the axios header
		setAuthorizationHeader(false);
		//  tell the system what happened
		dispatch(clearUser());
	}
};


export function clearUser() {
	return { type: Enums.User.event.logout };
}

export function setCurrentUser(dispatch){
	return axios.get('/api/me').then(resp => {
		const user = resp.data;
		dispatch({ type: Enums.User.event.login, user });
		return user;
	});
}

export function processToken(token){
	return dispatch => {
		const user = jwt.decode(token);
		// init with user in token for isAuth
		dispatch({ type: Enums.User.event.login, user });
		// jwt tokens sends expired in seconds, make date now seconds
		const expiredToken = user.exp < (Date.now()/1000);
		// if the token is not expired
		if(!expiredToken) {
			setAuthorizationHeader(token);
			setCurrentUser(dispatch);
		}
		else {
			setAuthorizationHeader(false);
			dispatch({type: Enums.User.event.logout});
		}
	}
};


// COURSES //
export function courseAdded(course){
	return {type: Enums.Me.courseAdded, course};
};

export function courseEdited(course){
	return {type: Enums.Me.courseEdited, course};
};

export function courseRemoved(course){
	return {type: Enums.Me.courseRemoved, course};
};

export function coursesFetched(courses){
	return {type: Enums.Me.coursesFetched, courses};
};

// MEALS //
export function mealAdded(meal) {
	return { type: Enums.Me.mealAdded, meal };
};

export function mealRemoved(meal) {
	return { type: Enums.Me.mealRemoved, meal };
};

export function mealsFetched(meals) {
	return { type: Enums.Me.mealsFetched, meals };
};