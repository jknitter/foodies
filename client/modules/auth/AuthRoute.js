import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const AuthRoute = ({component:Component, auth, ...rest}) => {
    return <Route { ...rest } render={(props) => (
		auth ? <Component {...props} /> : <Redirect to="/login" />
	)} />;
};

export default AuthRoute;