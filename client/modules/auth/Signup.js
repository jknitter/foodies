import React from 'react';
import axios from 'axios';
import history from '../core/history';
import TextWithLabel from '../components/TextWithLabel';
import { validateSignup } from '../../../shared/validate';

export default class Signup extends React.Component {
	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);	
	}

	componentWillMount(){
		this.setState({ 
			isLoading: false, errors: {}, 
			user: { userId: '', name: '', zip: '', email:'', pwd1: '', pwd2: ''}
		});
	}

	isValid(){
		const { errors, isValid } = validateSignup(this.state.user);
		if(!isValid)
			this.setState({errors});

		return isValid;
	}

	onSubmit(e){
		this.setState({errors: {}, isLoading: true});
		e.preventDefault();

		if(this.isValid()){
			const {isLoading, errors, user} = this.state;

			axios.post('/api/auth/signup', {user: user})
			.then((user) => { 
				this.setState({isLoading: false});
				history.push('/login');
			 })
			.catch((xhr) => { 
				if(xhr.response)
					this.setState({errors: xhr.response.data, isLoading: false});
				else
					this.setState({errors: { userId: xhr.message}, isLoading: false});
			});
		}
		else {
			this.setState({ isLoading: false });
		}
	}

	onChange(e){
		const user = {...this.state.user, [e.target.name]: e.target.value }
		this.setState(...this.state, { user: user });
	}

	render(){
		const { errors, isLoading, user } = this.state;

		return (
			<form className='signup-form' onSubmit={this.onSubmit.bind(this)}>
				<center><h3>Sign Up</h3></center>
				<TextWithLabel
					error={errors.userId}
					label='User Id'
					name='userId' 
					onChange={this.onChange}
					value={user.userId}
					maxLength="10" 
					autoFocus
				/>

				<TextWithLabel
					error={errors.name}
					label='Display Name'
					name='name' 
					onChange={this.onChange}
					value={user.name} 
					maxLength="10"
				/>

				<TextWithLabel
					error={errors.zip}
					label='Zipcode'
					name='zip' 
					onChange={this.onChange}
					value={user.zip} 
					maxLength="5"
				/>

				<TextWithLabel
					error={errors.email}
					label='Email'
					name='email' 
					onChange={this.onChange}
					value={user.email}
					maxLength="50"
				/>

				<TextWithLabel
					error={errors.pwd1}
					label='Password'
					name='pwd1' 
					onChange={this.onChange}
					type="password"
					value={user.pwd1} 
					maxLength="15"
				/>

				<TextWithLabel
					error={errors.pwd2}
					label='Confirm Password'
					name='pwd2' 
					onChange={this.onChange}
					type="password"
					value={user.pwd2} 
					maxLength="15"
				/>

				<button disabled={this.state.isLoading} type="submit">Submit</button>
			</form>
		);
	}
}