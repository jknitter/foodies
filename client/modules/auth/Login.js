import React from 'react';
import axios from 'axios';

import history from '../core/history';

import TextWithLabel from '../components/TextWithLabel';
import { validateLogin } from '../../../shared/validate';
import { login } from '../actions/user';

export default class Login extends React.Component {
	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);	
	}

	componentWillMount(){
		this.setState({ 
			isLoading: false, errors: {}, user: { userId: '', pwd1: ''}
		});
	}

	isValid(){
		const { errors, isValid } = validateLogin(this.state.user);
		if(!isValid)
			this.setState({errors});

		return isValid;
	}

	onSubmit(e){
		this.setState({errors: {}, isLoading: true});
		e.preventDefault();

		if(this.isValid()){
			const {isLoading, errors, user} = this.state;

			axios.post('/api/auth/login', user)
			.then(resp => {
				this.props.dispatch( login(resp.data.token) )
					.then(loginResp => { 
						this.setState({isLoading: false}); 
						history.replace('/me/profile');
					})
					.catch((xhr) => { 
						this.setState({errors: xhr.response.data.errors, isLoading: false}) 
					})
			});
		}
		else {
			this.setState({ isLoading: false });
		}
	}

	onChange(e){
		const user = {...this.state.user, [e.target.name]: e.target.value }
		this.setState(...this.state, { user: user });
	}

	render(){
		const { errors, isLoading, user } = this.state;

		return (
			<form className='login-form' onSubmit={this.onSubmit.bind(this)}>
				<center><h3>Login</h3></center>
				<TextWithLabel
					error={errors.userId}
					label='User Id'
					name='userId' 
					onChange={this.onChange}
					value={user.userId}
					autoFocus
				/>

				<TextWithLabel
					error={errors.pwd1}
					label='Password'
					name='pwd1' 
					onChange={this.onChange}
					type="password"
					value={user.pwd1} 
				/>

				{errors.err && <span className="err-block">*{errors.err}</span>}
				<button disabled={this.state.isLoading} type="submit">Submit</button>
			</form>
		);
	}
}