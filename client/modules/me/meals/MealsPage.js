import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

import Overlay from '../../components/Overlay';
import MealCard from './MealCard';
import MealForm from './MealForm'
import { mealsFetched } from '../../actions/user';
import { validateProfile } from '../../../../shared/validate';

@connect(store => {
    return {
        user: store.user.user
    };
})
export default class MealsPage extends React.Component {
    constructor(props) { super(props); }

    mealFormClose() {
        this.setState({ showMealForm: false });
    }

    componentWillMount() {
        // if show === true it's adding, if its an obj, that the obj to edit
        this.setState({ errors: {}, showMealForm: false });

        axios.get('/api/me/meals')
            .then(resp => this.props.dispatch(mealsFetched(resp.data)))
            .catch(err => console.log('cookbook.willmount.catch', err))
    }

    // edit a meal in the cookbook
    edit(c, e) {
        e.stopPropagation();
        this.setState({ errors: {}, showMealForm: c });
    }

    render() {
        const mealEls = this.props.user.meals.map((c, i) => {
            return <MealCard
                selected={this.state.showMealForm === c}
                edit={this.edit.bind(this, c)}
                label={c.name}
                key={i}
            />
        });

        const title = (this.state.showMealForm === true ? 'Add' : 'Edit') + ' Meal';
        // important to use undefined, not null, here. The typeof for null is object
        const editObj = typeof (this.state.showMealForm) === 'object' ? this.state.showMealForm : undefined;
        
        const noLen = <div class="none">
            <div class="action">Get started by adding a meal</div>
            <div class="flex"> 
                <div class="tip">
                    <i class="fa fa-th-list"/>
                    <div>Orgnize courses you have entered into meals</div>
                </div>
                <div class="tip">
                    <i class="fa fa-newspaper-o" />
                    <div>Scheduled for a limited time or put them on your regular menu</div> 
                </div>
                <div class="tip">
                    <i class="fa fa-pie-chart" />
                    <div>Determine pricing, quanties avalible, and logistics.</div>
                </div>
            </div>
        </div>

        const overlay = <Overlay title={title} close={this.mealFormClose.bind(this)}>
            <MealForm
                meal={editObj}
                close={this.mealFormClose.bind(this)}
                dispatch={this.props.dispatch}
            />
        </Overlay>;

        return (
            <div class="meals">
                <div class="top">
                    <div class="title">My Meals</div>
                    <ul>
                        <li class='icon-w-text' onClick={() => this.setState({ showMealForm: true })}>
                            <i class="fa fa-plus" />Add
                        </li>
                    </ul>
                </div>
                <div class='meal-list'>
                    {mealEls.length === 0 && noLen}
                    {mealEls.length > 0 && mealEls}
                </div>
                {this.state.showMealForm && overlay}
            </div>
        );
    }
};