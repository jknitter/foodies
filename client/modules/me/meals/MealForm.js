import React from 'react';
import axios from 'axios';

import Pics from '../../components/Pics';
import TextWithLabel from '../../components/TextWithLabel';
import TagInput from '../../components/TagInput';

import { validateMeal } from '../../../../shared/validate';
import { mealAdded, mealEdited, mealRemoved } from '../../actions/user';

let fileInput;

export default class CourseForm extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        fileInput = document.querySelector('input[type=file]');
    }

    componentWillMount() {
        
        const defaultMeal = {
            name: '',
            portionSize: 0,
            pics: [],
            portionUnits: '',
            ingredients: []
        };

        this.setState({
            errors: {},
            isLoading: false,
            meal: this.props.meal || defaultMeal,
            // client-side url blob for images that have been selected in UI but not yet saved to the course
            uploadingPics: []
        });
    }

    // remove a meal from the cookbook
    delete(e) {
        e.stopPropagation();
        let c = this.state.meal;
        axios.delete('/api/me/meals/' + c._id)
            .then(resp => {
                this.props.close();
                this.props.dispatch(mealRemoved(c))
            })
            .catch(err => console.log('mealform.delete.catch', err))
    }

    isValid() {
        const { errors, isValid } = validateMeal(this.state.meal);
        if (!isValid)
            this.setState({ errors });

        return isValid;
    }

    onChange(e) {
        const meal = { ...this.state.meal, [e.target.name]: e.target.value }
        this.setState(...this.state, { meal: meal });
    }

    onSubmit(e) {
        this.setState({ errors: {}, isLoading: true });
        e.preventDefault();

        if (this.isValid()) {
            // const { meal } = this.state;
            // const upNames = Array.from(uploadingPics).map(f => f.name);
            // const pics = new Set(meal.pics.concat(upNames));

            let method = this.props.meal ? axios.put : axios.post;
            method('/api/me/meals', Object.assign({}, meal))
                .then(async saveResp => {
                    // new meals only have an id after save, pics needs a meal id to save
                    // if (uploadingPics.length > 0) {
                    //     const list = await this.uploadPics(saveResp.data, uploadingPics);
                    //     fileInput.value = '';
                    //     this.setState({ uploadingPics: [] });
                    // }

                    return saveResp.data;
                })
                .then(savedMeal => {
                    // if edit
                    const actionFn = this.props.meal ? mealEdited : mealAdded;
                    this.props.dispatch(actionFn(savedMeal));
                    this.props.close();
                })
                .catch(xhr => {
                    if (xhr.response)
                        this.setState({ errors: xhr.response.data, isLoading: false });
                    else
                        this.setState({ errors: { userId: xhr.message }, isLoading: false });
                });
        }
        else
            this.setState({ isLoading: false });
    }

    render() {
        const errors = {};
        const meal = {};

        return (
            <div class="meal-form-container">
                <form className='meal-form'>
                    <TextWithLabel
                        error={errors.price}
                        label="Price"
                        name="price"
                        onChange={this.onChange}
                        value={meal.price}
                        maxLength="20"
                        pattern="(d*)([.])(d{2})"
                        cls="price"
                    />

                    <TextWithLabel
                        error={errors.portionSize}
                        label="Portion Size"
                        name="portionSize"
                        onChange={this.onChange}
                        value={meal.portionSize}
                        maxLength="5"
                        cls="size"
                    />

                    <TagInput
                        label="Units"
                        type="portionUnits"
                        error={errors.portionUnits}
                        onChange={this.onChange}
                        tags={[meal.portionUnits]}
                        maxTags={1}
                        maxLength="10"
                        cls="units"
                    />
                </form>
                <ul>
                    {this.props.meal && <li class="icon-w-text" onClick={this.delete.bind(this)}>
                        <i class="fa fa-trash"></i>
                        Delete
                    </li>}
                    <li class="icon-w-text" onClick={this.onSubmit.bind(this)}>
                        <i class="fa fa-floppy-o"></i>
                        Save
                    </li>
                </ul>
            </div>
        );
    }
};