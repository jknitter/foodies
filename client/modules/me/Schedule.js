import React from 'react';

export default class Profile extends React.Component {
    constructor(props){ 
        super(props);
    }
    render(){
        return ( <div class="schedule-content">
            <h4>Your menu</h4>
            <div class="font-size-medium">Made To Order</div>
            <div>
                Any courses or meals you have entered or imported can be marked as "Made to Order". 
                This will let customers order the course separate from a planned meal.
            </div>
            
            <div class="font-size-medium margin-top-large">Planned Meals</div>
            <div>Make extra portions of meals you are alreadly planning on making</div>
        </div> )
    }
}