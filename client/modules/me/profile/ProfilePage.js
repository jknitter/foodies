import React from 'react';
import axios from 'axios';

// app wide
export default class ProfilePage extends React.Component {
	constructor(props){
		super(props);
	}
	
	closeForm(){
		this.setState({openForm: null});
	}

	componentWillMount(){
		const user = this.props.user;
		let avatar = user.avatar;
		if(avatar)
			avatar = `/api/image/${user._id}/avatar.jpg`;
		
		this.setState({ openForm: null, avatar: avatar });		
	}

	picSelected(){
		const fileInput = document.querySelector('input[type=file]');
		if(fileInput.files.length === 0) return;

		const reader = new FileReader();
		const file = fileInput.files[0];
		reader.readAsDataURL(file);
		reader.onload = e => {
			const src = e.target.result;
			this.setState({avatar: src});
			axios.post(`/api/me/avatar`, {name: file.name, src: src});
		}
	}

	removeAvatar(e){
		e.stopPropagation();

		axios.delete(`/api/me/avatar`)
			.then(() => this.setState({avatar: false}))
			.catch(err => this.setState({ errors: { deleteAvatar: err }}));
	}

	render(){
		const { errors={}, user } = this.props;
		const loc = user.location;
		const locStr = `${loc.placename || ''}, ${loc.state || ''} ${loc.zipcode || ''}`;

		return ( <div class="profile-page"> 
			<div class="top">
				<div class="title">Profile</div>
			</div>
			<div class="avatar" onClick={() => document.querySelector('input[type=file]').click()}>
				<input type="file" accept="image/*" style={{"display": "none"}} onChange={this.picSelected.bind(this)} />
				{!this.state.avatar && <div class="none">
					<i class="fa fa-id-badge" />
					<br/>Add Profile Pic
				</div>}
				
				{ this.state.avatar && <img src={this.state.avatar} /> }
			</div>
			{ this.state.avatar && 
				<button class="remove-avatar" onClick={this.removeAvatar.bind(this)}>
					<i class="fa fa-trash" />
					Remove
				</button>
			}
		</div> )
	}
}