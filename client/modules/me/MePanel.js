import React from 'react';
import { Link } from 'react-router-dom';

import history from '../core/history';
let unlisten;

export default class MePanel extends React.Component {
	componentWillMount(){
		console.log(history.location.pathname.split('/'))
		this.setState({ loc: history.location.pathname.split('/')[2] });

		// Listen for changes to the current location.
		unlisten = history.listen((location, action) => {
			this.setState({ loc: location.pathname.split('/')[2] });
		});
	}

	componentWillUnmount(){
		if(unlisten) unlisten();
	}

	getNavButtonClassName(name){
		let str = '';
		// ignore the first, path always starts with a slash
		if(this.state.loc === name) str += 'active ';
		return str;
	}

	render(){
		return <div className="me-panel">
			<ul>
				<li className={this.getNavButtonClassName('profile')} onClick={()=> history.push('/me/profile')}>
					<i class="fa fa-address-book-o" />Profile
				</li>
				<li className={this.getNavButtonClassName('courses')} onClick={()=> history.push('/me/courses')}>
					<i class="fa fa-book" />Courses
				</li>
				<li className={this.getNavButtonClassName('meals')} onClick={() => history.push('/me/meals')}>
					<i class="fa fa-cutlery"/>Meals
				</li>
				<li className={this.getNavButtonClassName('schedule')} onClick={()=> history.push('/me/schedule')}>
					<i class="fa fa-calendar" />Schedule
				</li>
				<li className={this.getNavButtonClassName('account')} onClick={()=> history.push('/me/account')}>
					<i class="fa fa-user-circle" />Accounting
				</li>
				<li className={this.getNavButtonClassName('prefs')} onClick={()=> history.push('/me/settings')}>
					<i class="fa fa-gears" />Settings
				</li>
			</ul>
		</div>
	}
};