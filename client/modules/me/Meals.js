import React from 'react';
import axios from 'axios';
import history from '../core/history';

import LabeledDrawer from '../components/LabeledDrawer';

import Course from '../me/Course';

import { coursesFetched, courseRemoved } from '../actions/me';

export default class Meal extends React.Component {
	constructor(props){ super(props); }
	
	remove(c){
		axios.delete('/api/me/course/' + c._id)
			.then(resp => this.props.dispatch(courseRemoved( c )))
			.catch(err => console.log('meals.meal.remove.catch', err))
	}
	
	render() {
		const courses = this.props.courses.map((c, i) => {
            return <LabeledDrawer 
                component={Course}
				remove={this.remove.bind(this, c)}
				label={c.displayName} 
				key={i} 
				{...c}
			/>
		});

		return <div class='meal-container'>
			{this.props.cookName}
			{courses.length === 0 && <div style={{ textAlign: 'center' }}>
				No courses in this meal, let's add some
				<ul style={{marginTop:'5px'}}>
					<li class='icon-w-text'>
						<i class="fa fa-plus" />Add Course
					</li>
				</ul>
			</div>}
			{courses}
		</div>
	}
}