import React from 'react';
import { Redirect, Route } from 'react-router-dom';

import { connect } from 'react-redux';
import history from '../core/history';

// home content components
import Account from '../me/Account';
import CoursesPage from '../me/courses/CoursesPage';
import ProfilePage from '../me/profile/ProfilePage';
import SettingsPage from '../me/settings/SettingsPage';
import MealsPage from '../me/meals/MealsPage';
import Schedule from '../me/Schedule';

@connect(store => {
	return {
		user: store.user.user
	};
})
export default class Me extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (<div class="me-page">
			<Route exact path="/me" component={() => <Redirect to={'/me/profile'} />} />
			<Route path="/me/account" component={Account} />
			<Route path="/me/courses" component={CoursesPage} />
			<Route path="/me/meals" component={MealsPage} />
			<Route path="/me/settings" component={() => <SettingsPage dispatch={this.props.dispatch} user={this.props.user} />} />
			<Route path="/me/profile" component={() => <ProfilePage dispatch={this.props.dispatch} user={this.props.user} />} />
			<Route path="/me/schedule" component={Schedule} />
		</div>)
	}
}
