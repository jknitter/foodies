import React from 'react';
import classnames from 'classnames';

import Tag from '../../components/Tag';

export default class CourseCard extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const editBtn = (
			<li class='icon-w-text' onClick={this.props.edit}>
				<i class="fa fa-pencil" />
			</li>
		);

		const rmvBtn = (
			<li class='icon-w-text' onClick={this.props.remove}>
				<i class="fa fa-trash-o" />
			</li>
		);

		const ingredientTags = this.props.ingredients.map((ing, i)=> {
			return <Tag key={i} tag={ing} /> 
		});

		return (
			<div className={classnames('course-card-container', { 'selected': this.props.selected })} onClick={(e) => this.props.edit(e)} >
				<div class="pic">
					{this.props.pics && this.props.pics.length > 0 && <img src={`/api/image/${this.props.cookId}/courses/${this.props._id}/${this.props.pics[0]}`} />}
					{this.props.pics && this.props.pics.length === 0 && <i class="fa fa-image"></i>}
				</div>
				<div class="name">
					{this.props.name}
				</div>
				<div class="description">
					{!this.props.description && <span>- No description added -</span>}
					{this.props.description && <span>{this.props.description}</span>}
				</div>
				<div class="ingredients">
					{ingredientTags.length === 0 && <span>- No ingredients added -</span>}
					{ingredientTags}
				</div>
			</div>
		)
	}
};
