import React from 'react';
import CourseForm from './CourseForm';
export default (props) => {
	return (
		<div class="course-container">
			<div class="course-view-container">
				<div class="left-row">
					<div class="portion-price">
						{props.portionSize + props.portionUnits} for ${props.price}
					</div>
					
					<div class="course-pics">
						<i class="fa fa-image"></i>
					</div>
				</div>
					
				<div class="right-row">
					<label>Description</label>
					<div>{props.description}</div>
					
					<label>Tags</label>
					<div>{props.tags}</div>

					<label>Ingredients</label>	
				</div>
			</div>
		</div>
	);
}
