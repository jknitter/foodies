import React from 'react';
import axios from 'axios';

import Pics from '../../components/Pics';
import TextWithLabel from '../../components/TextWithLabel';
import TagInput from '../../components/TagInput';

import { validateCourse } from '../../../../shared/validate';
import { courseAdded, courseEdited, courseRemoved } from '../../actions/user';

let fileInput;

export default class CourseForm extends React.Component {
   constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount(){
        fileInput = document.querySelector('input[type=file]');
    }

    componentWillMount(){           
        const defaultCourse = {
            name: '',
            pics: [],
            ingredients: []
        };

        this.setState({errors: {},
            isLoading: false,
            course: this.props.course || defaultCourse,
            // client-side url blob for images that have been selected in UI but not yet saved to the course
            uploadingPics: [] 
        });
    }

    // remove a course from the cookbook
    delete(e) {
        e.stopPropagation();
        let c = this.state.course;
        axios.delete('/api/course/' + c._id)
            .then(resp => {
                this.props.close();
                this.props.dispatch(courseRemoved(c))
            })
            .catch(err => console.log('cookbook.delete.catch', err))
    }

    isValid(){
        const { errors, isValid } = validateCourse(this.state.course);
        if (!isValid)
            this.setState({ errors });
        
        return isValid;
    }

    onChange(e) {
        const course = { ...this.state.course, [e.target.name]: e.target.value }
        this.setState(...this.state, { course: course });
    }

    // update ui
    picsRemoved(index){
        const { course } = this.state;
        let newPics;
        // the pics displayed consist of images that are saved and those pending upload
        // if the removed pic was already saved
        if(index < course.pics.length){
            newPics = Array.from(course.pics);
            newPics.splice(index, 1);

            this.setState({
                ...this.state,
                course: { ...this.state.course, pics: newPics }
            });            
        }
        // check the pending uploads
        else {
            // update the index to exclude the saved list, which comes first
            index -= course.pics.length;
            newPics = Array.from(this.state.uploadingPics);
            newPics.splice(index, 1);
            this.setState({uploadingPics: newPics});
        }
    }

    // update ui
    picsSelected() {
        if(fileInput.files.length === 0) return;

        // when new files selected, wipe out previous files
        this.setState({ uploadingPics: [] });

        // all the pics, but not the saved and pre-uploaded pics
        // so we overwrite any previously added
        const pics = this.state.course.pics;
        // push them into the pic carousel
        for(let i=0; i<fileInput.files.length; i++){
            // prevent duplicate file names
            const file = fileInput.files[i];
            if(pics.indexOf(file.name) === -1){
                const reader = new FileReader();
                let fileToUpload = file;
                reader.readAsDataURL( file );
                reader.onload = e => {
                    fileToUpload.src = e.target.result;
                    // uploadingPics hold the image blob url for a course before saving
                    this.setState({ uploadingPics: this.state.uploadingPics.concat([ fileToUpload ]) });
                };
            }
        };

        fileInput.value = '';
    }

    render() {
        const { errors, isLoading, course } = this.state;
        let picList = course.pics.map(fileName => `/api/image/${course.cookId}/courses/${course._id}/${fileName}`);
        picList = picList.concat( this.state.uploadingPics.map(up => up.src) );
      
        return ( 
            <div class="course-form-container">
                <form className='course-form'>
                    <TextWithLabel
                        error={ errors.name }
                        label='Name'
                        name='name'
                        onChange={ this.onChange }
                        value={ course.name }
                        maxLength="30"
                    />

                    <TagInput
                        label='Ingredients'
                        type='ingredients'
                        tags={course.ingredients}
                        onChange={this.onChange}
                        maxTags={3}
                        maxLength='15'
                    />

                    <TextWithLabel
                        error={errors.description}
                        label='Description'
                        name='description'
                        onChange={this.onChange}
                        value={course.description}
                        maxLength="200"
                        type="area" rows="4"
                    />

                    <div class="course-pics-container">
                        <label for="">Pics</label>
                        <Pics picList={picList} 
                            picsRemoved={this.picsRemoved.bind(this)} 
                            picsSelected={this.picsSelected.bind(this)} />
                    </div>
                </form>  
                
                <ul class="overlay-footer">
                    {this.props.course && <li class="active" onClick={this.delete.bind(this)}>
                        <i class="fa fa-trash" />Delete
                    </li>}
                    <li class="active" onClick={this.save.bind(this)}>
                        <i class="fa fa-save" />Save
                    </li>
                </ul>
            </div>
        );
    }

    save(e){
        this.setState({ errors: {}, isLoading: true });
        e.preventDefault();
        
        if(this.isValid()){
            const { course, uploadingPics } = this.state;
            const upNames = Array.from(uploadingPics).map(f => f.name);
            const pics = new Set( course.pics.concat(upNames) );
            
            const isEdit = !!this.props.course;
            const method =  isEdit ? axios.put : axios.post;

            const courseData = Object.assign({}, course, { pics: [...pics] });
            let postData = { course: courseData };
            if(isEdit) 
                postData.removed = this.props.course.pics.filter(p => course.pics.indexOf(p) === -1);
            
            // if new pics added
            if(uploadingPics.length > 0)
                postData.addedPics = uploadingPics.map(
                    up => new Object({ name: up.name, src: up.src })
                );

            method('/api/course', postData)
                .then(savedCourse => {
                    // if edit
                    const actionFn = this.props.course ? courseEdited : courseAdded;
                    this.props.dispatch(actionFn(savedCourse.data));
                    this.props.close();
                })
                .catch(xhr => {
                    if(xhr.response)
                        this.setState({ errors: xhr.response.data, isLoading: false });
                    else
                        this.setState({ errors: { userId: xhr.message }, isLoading: false });
                });
        }
        else
            this.setState({ isLoading: false });
    }
};