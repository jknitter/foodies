import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

import Overlay from '../../components/Overlay';
import Card from './CourseCard';
import CourseForm from './CourseForm'
import { coursesFetched } from '../../actions/user';
import { validateProfile } from '../../../../shared/validate';

@connect(store => {
    return {
        user: store.user.user
    };
})
export default class CoursesPage extends React.Component {
    constructor(props) { super(props); }

    courseFormClose() {
        this.setState({ showCourseForm: false });
    }

    componentWillMount() {
        // if show === true it's adding, if its an obj, that the obj to edit
        this.setState({ errors: {}, showCourseForm: false });

        axios.get('/api/me/courses')
            .then(resp => this.props.dispatch(coursesFetched(resp.data)))
            .catch(err => console.log('cookbook.willmount.catch', err))
    }

    // edit a course in the cookbook
    edit(c, e) {
        e.stopPropagation();
        this.setState({ errors: {}, showCourseForm: c });
    }

    render() {
        const courseEls = this.props.user.courses.map((c, i) => {
            return <Card
                selected={this.state.showCourseForm === c}
                edit={this.edit.bind(this, c)}
                {...c}
                key={i}
            /> 
        });

        const title = (this.state.showCourseForm === true ? 'Add' : 'Edit') + ' Course';
        // important to use undefined, not null, here. The typeof for null is object
        const editObj = typeof(this.state.showCourseForm) === 'object' ? this.state.showCourseForm : undefined;
        const noLen = <div class="none">
            <div class="action">Get started by adding courses</div>
            <div class="flex">
                <div class="tip">
                    <i class="fa fa-object-group" />
                    <div>Sell courses ala carte or as part of a meal</div>
                </div>
                <div class="tip">
                    <i class="fa fa-newspaper-o" />
                    <div>Scheduled for a limited time or put them on your regular menu</div>
                </div>
                <div class="tip">
                    <i class="fa fa-pie-chart" />
                    <div>Determine pricing, quanties avalible, and logistics.</div>
                </div>
            </div>
        </div>

        const formConfig = {
            course: editObj,
            close: this.courseFormClose.bind(this),
            dispatch: this.props.dispatch
        };
        const overlay = <Overlay 
                            bgClickClose={true} 
                            title={title} 
                            close={this.courseFormClose.bind(this)}
                            comp={CourseForm}
                            config={formConfig} />;

        return ( 
            <div class="courses">
                <div class="top">
                    <div class="title">My Courses</div> 
                    <ul>
                        <li class='icon-w-text' onClick= {() => this.setState({ showCourseForm: true })}>
                            <i class = "fa fa-plus"/>Add
                        </li> 
                    </ul>
                </div>
                <div class='course-list'>
                    { courseEls.length === 0 && noLen } 
                    { courseEls.length > 0 && courseEls } 
                </div>
                { this.state.showCourseForm && overlay }
            </div>
        );
    }
};