import React from 'react';
import axios from 'axios';

import Enums from '../../core/Enums';

import TextWithLabel from '../../components/TextWithLabel';
import { validateProfileLocation } from '../../../../shared/validate';

export default class ProfileLocationForm extends React.Component {
    constructor(props){
        super(props)
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount(){
        const {dispatch, close, ...rest} = this.props;
        this.setState({location: rest, errors: {}});
    }

    isValid() {
        const { errors, isValid } = validateProfileLocation(this.state.location);
        if(!isValid)
            this.setState({ errors });

        return isValid;
    }

    onChange(e){
        this.setState({
            location: {...this.state.location, [e.target.name]: e.target.value }
        });
    }

    save(){
        if(this.isValid()){
            axios.put('/api/me/location', this.state.location)
                .then(resp => 
                    this.props.dispatch({ 
                        type: Enums.User.event.locationFetched, 
                        payload: this.state.location 
                    })
                );
        }
    }

    render(){
        const { errors } = this.state;
      
        return <div class="profile-location-form-container"> 
            <form class="profile-location-form">
                <TextWithLabel
                    error={errors.address}
                    label='Address'
                    name='address'
                    onChange={this.onChange}
                    value={this.state.location.address}
                    maxLength="50"
                    cls="address"
                />

                <TextWithLabel
                    error={errors.placename}
                    label='City'
                    name='placename'
                    onChange={this.onChange}
                    value={this.state.location.placename}
                    maxLength="50"
                    cls="city"
                />

                <TextWithLabel
                    error={errors.state}
                    label='State'
                    name='state'
                    onChange={this.onChange}
                    value={this.state.location.state}
                    maxLength="50"
                    cls="state"
                />

                <TextWithLabel
                    error={errors.zipcode}
                    label='Zipcode'
                    name='zipcode'
                    onChange={this.onChange}
                    value={this.state.location.zipcode}
                    maxLength="50"
                    cls="zip"
                />
            </form>
            
            <ul class="overlay-footer">
                <li class="active" onClick={this.props.close}>
                    <i class="fa fa-trash" />Cancel
                </li>
                <li class="active" onClick={this.save.bind(this)}>
                    <i class="fa fa-save" />Save
                </li>
            </ul>
        </div>
    }
};