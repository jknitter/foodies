import React from 'react';
import axios from 'axios';

import LocationForm from "./LocationForm";
import Overlay from '../../components/Overlay';

import TextWithLabel from '../../components/TextWithLabel';
import { validateProfile } from '../../../../shared/validate';
import ChangePasswordForm from './ChangePasswordForm';

// app wide
// metric system or imperial

//search
// default search radidus


export default class AccountSettingsPage extends React.Component {
	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);
	}
	
	closeForm(){
		this.setState({openForm: null});
	}

	componentWillMount(){
		this.setState({ 
			openForm: null,
			user: {...this.props.user}, 
			errors: {} });
	}

	isValid(){
		const { errors, isValid } = validateProfile(this.state.user);
		if(!isValid)
			this.setState({errors});

		return isValid;
	}

	onChange(e){
		const user = {...this.state.user, [e.target.name]: e.target.value }
		this.setState(...this.state, { user: user });
	}

	render(){
		const { errors, user } = this.state;
		const loc = user.location;
		const locStr = `${loc.placename || ''}, ${loc.state || ''} ${loc.zipcode || ''}`;

		const changePwdOverlay = <Overlay
			title={'Change Password'}
			close={this.closeForm.bind(this)}
			comp={ChangePasswordForm}
		/>

		const editLocationOverlay = <Overlay
			title={'Location'}
			close={this.closeForm.bind(this)}
			comp={LocationForm}
			config={Object.assign({}, this.props.user.location, { dispatch: this.props.dispatch })} />

		return (
			<div class="account-settings-page">
				<form class="account-section">
					<TextWithLabel
						disabled={true}
						label='User Id'
						name='userId' 
						value={user.userId}
						maxLength="10" 
						readOnly={true}
						cls="userId"
					/>

					<TextWithLabel
						error={errors.name}
						label='Display Name'
						name='name' 
						onChange={this.onChange}
						value={user.name} 
						maxLength="10"
						cls="name"
					/>

					<TextWithLabel
						error={errors.phone}
						label='Phone'
						name='phone'
						onChange={this.onChange}
						value={user.phone}
						maxLength="9"
						placeholder="5551234567"
						cls="phone"
					/>

					<TextWithLabel
						error={errors.email}
						label='Email'
						name='email' 
						onChange={this.onChange}
						value={user.email}
						maxLength="50"
						cls="email"
					/>
				</form>
				
				<div class="location-section">
					<h3>Location</h3>
					<div class="margin-bottom">
						{loc.address && loc.address.length > 0 && `${loc.address}	`}
						{locStr.length > 1 && locStr}
					</div>
					<button onClick={() => this.setState({ openForm: editLocationOverlay })} >
						<i class="fa fa-pencil" />
						Edit
						</button>
				</div>

				<h3>Security</h3>
				<form class="security-section">
					<div class="pwd">
						<ul><li class="active" onClick={() => this.setState({ openForm: changePwdOverlay })} >
							Change Password
						</li></ul>
					</div>
					
					<div>
						show phone, show email never, when someone is ordering, always
					</div>
				</form>

				<h3>Cooking</h3>
				<form class="cooking-section">
					<div>
						You are not signed up as a cook.
						or
						Status: none | reviewing | approved
						then
						Hours I cook : 11am - 8pm	
					</div>
				</form>

				<h3>General</h3>
				<form class="general-section">	
					<TextWithLabel
						label='currency'
						value='dollar'
						maxLength="5"
						readOnly={true}
					/>
					
					<TextWithLabel
						label='etc'
						value='22'
						maxLength="5"
						readOnly={true}
					/>
				</form>
				{ this.state.openForm !== null && this.state.openForm }
			</div>
		);
	}
};