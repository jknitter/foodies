import React from 'react';
import axios from 'axios';

import Overlay from '../../components/Overlay';

import TextWithLabel from '../../components/TextWithLabel';
import { validateProfile } from '../../../../shared/validate';

// app wide
// metric system or imperial

//search
// default search radidus


export default class PrefsSettingsPage extends React.Component {
	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);
	}

	componentWillMount(){
		this.setState({ user: {...this.props.user}, errors: {} });
	}

	isValid(){
		const { errors, isValid } = validateProfile(this.state.user);
		if(!isValid)
			this.setState({errors});

		return isValid;
	}

	onChange(e){
		const user = {...this.state.user, [e.target.name]: e.target.value }
		this.setState(...this.state, { user: user });
	}

	onSubmit(e){
		this.setState({errors: {}, isLoading: true});
		e.preventDefault();

		if(this.isValid()){
			// basically remove isloading and errors before sending to server
			//TODO bad idea change this
			const {isLoading, errors, user} = this.state;

			axios.post('/api/auth/signup', {user: user})
			.then(user => this.setState({isLoading: false}))
			.catch(xhr => { 
				if(xhr.response)
					this.setState({errors: xhr.response.data, isLoading: false});
				else
					this.setState({errors: { userId: xhr.message}, isLoading: false});
			});
		}
		else {
			this.setState({ isLoading: false });
		}
	}

	render(){
		const { errors, user } = this.state;
		return (
			<div class="prefs-page">
				<h3>Account</h3>
				<form class="account-section">
					<TextWithLabel
						disabled={true}
						label='User Id'
						name='userId' 
						readOnly={true}
						value={user.userId}
						maxLength="10" 
						cls="userId"
					/>

					<TextWithLabel
						error={errors.name}
						label='Display Name'
						name='name' 
						onChange={this.onChange}
						value={user.name} 
						maxLength="10"
						cls="name"
					/>

					<TextWithLabel
						error={errors.phone}
						label='Phone'
						name='phone'
						onChange={this.onChange}
						value={user.phone}
						maxLength="9"
						placeholder="5551234567"
						cls="phone"
					/>

					<TextWithLabel
						error={errors.email}
						label='Email'
						name='email' 
						onChange={this.onChange}
						value={user.email}
						maxLength="50"
						cls="email"
					/>

					<div class="location-section">
						<h3>Location</h3>
						<div class="margin-bottom">
							{loc.address && loc.address.length > 0 && `${loc.address}	`}
							{locStr.length > 1 && locStr}
						</div>
						<button onClick={() => this.setState({
							openForm: <Overlay
								title={'Location'}
								close={this.closeForm.bind(this)}
								comp={LocationForm}
								config={Object.assign({}, this.props.user.location, { dispatch: this.props.dispatch })} />
						})} >
							<i class="fa fa-pencil" />
							Edit
						</button>
					</div>
				</form>

				
				<h3>Security</h3>
				<form class="security-section">
					<div class="pwd">
						<ul><li class="active" onClick="">Change Password</li></ul>
					</div>
					
					<div>
						show phone, show email never, when someone is ordering, always
					</div>
				</form>


				<h3>Cooking</h3>
				<form class="cooking-section">
					<div>
						You are not signed up as a cook.
						or
						Status: none | reviewing | approved
						then
						Hours I cook : 11am - 8pm	
					</div>
				</form>

				<h3>General</h3>
				<form class="general-section">	
					<TextWithLabel
						label='currency'
						value='dollar'
						maxLength="5"
					/>
					
					<TextWithLabel
						label='etc'
						value='22'
						maxLength="5"
					/>
				</form>
			</div>
			);
		}
	};