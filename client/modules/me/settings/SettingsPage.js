import React from 'react';
import axios from 'axios';
import {Switch, Route, Redirect} from 'react-router-dom';
import classnames from 'classnames';
import history from '../../core/history';

import AccountPage from './AccountPage';
import PrefsPage from "./PrefsPage";

export default class SettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.navClick = this.navClick.bind(this);   
    }

    navClick(){
        console.log('nac bitch');
    }

    render() {
        const path = history.location.pathname;
        const aCls = 'flex-sm-fill text-sm-center nav-link';

        return (<div class="settings-page">
            <nav class="nav nav-pills flex-column flex-sm-row margin-bottom">
                <a onClick={this.navClick}
                   className={classnames(aCls, { 'active': path === '/me/settings/account' })} 
                   href="#">Account
                </a>
                <a onClick={this.navClick}
                   className={classnames(aCls, { 'active': path === '/me/settings/prefs' })} 
                   href="#">Prefs
                </a>
            </nav>

            <Switch>
                <Route exact path="/me/settings" component={() => <Redirect to="/me/settings/account" />} />
                <Route path="/me/settings/prefs" component={() => <PrefsPage dispatch={this.props.dispatch} user={this.props.user} />} />
                <Route path="/me/settings/account" component={() => <AccountPage dispatch={this.props.dispatch} user={this.props.user} />} />
            </Switch>
        </div>)
    }
};