import React from 'react';
import axios from 'axios';

import TextWithLabel from '../../components/TextWithLabel';
import Form from '../../components/Form';
import { validateChangePwd } from '../../../../shared/validate';

export default class ChangePasswordForm extends React.Component {
    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount(){
        const { dispatch, close, ...rest } = this.props;
        this.setState({ location: rest, errors: {} });
    }

    isValid(){
        const { errors, isValid } = validateChangePwd(this.state.location);
        if(!isValid)
            this.setState({ errors });

        return isValid;
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    save(){
        if (this.isValid()) {
            axios.post('/api/me/changePwd', this.state)
                .then(resp =>
                    this.props.dispatch({
                        type: Enums.User.event.locationFetched,
                        payload: this.state.location
                    })
                );
        }
    }

    render(){
        const { errors } = this.state;
        const form = <form class="change-passord-form">
            <TextWithLabel
                error={errors.current}
                label='Current Password'
                name='current'
                onChange={this.onChange}
                value={this.state.current}
                maxLength="20"
            />

            <TextWithLabel
                error={errors.newPwd}
                label='New Password'
                name='newPwd'
                onChange={this.onChange}
                value={this.state.newPwd}
                maxLength="20"
            />

            <TextWithLabel
                error={errors.confirmPwd}
                label='Confirm Password'
                name='confirmPwd'
                onChange={this.onChange}
                value={this.state.confirmPwd}
                maxLength="20"
            />
        </form>

        return <Form 
                form={form}
                save={this.save.bind(this)}
                close={this.props.close}
        />
    }
};