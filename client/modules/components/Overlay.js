import React from 'react';

/**
 * If Overlay will render comp property then any children
 * Overlay Component
 * 	optional props
 * 		object - 
 * 			comp - the component to diplay in the Overlay
 * 		fns -
 * 			save, delete
 * 		bool -
 * 			bgClickDontClose -  prevent close if user clicks on overlay bg (default: false)
 * 			hideClose - hide close button
 * 			hideTitle - hide title element
 * 			closeOnSelect - click passes throught to bg and causes closed to be called
 * 		string - 
 * 			align - left, right - (default) centered dynamic width
 * 			cls - class name(s) to add to container
 */
export default class Overlay extends React.Component {
	constructor(props){
		super(props);
		if(!props.close && (this.props.bgClickDontClose !== true || !this.props.hideClose)) 
			console.error('No close function for Overlay');
	}

	render(){
		const hideTitle = !this.props.title;
		const showClose = this.props.hideClose !== true;;	
		
		let compCls = "component-container";
		if(this.props.cls) compCls += ` ${this.props.cls}`;
		if(this.props.align) compCls += ` ${this.props.align}`;

		return <div class="overlay-container" onClick={() => {if(this.props.bgClickDontClose !== true) this.props.close()}}>
			<div class={compCls} onClick={e => { if(!this.props.closeOnSelect) e.stopPropagation(); }}>
				{!hideTitle && showClose && <div class="overlay-title">
					{!hideTitle && <div class="title">{this.props.title}</div>}
					{showClose && <i class="fa fa-close btn-secondary icon-w-text" onClick={this.props.close} />}
				</div>}
				{this.props.comp && <this.props.comp {...this.props.config} close={this.props.close}/>}
				{this.props.children}
			</div>
		</div>
	}
};
