import React from 'react';
import axios from 'axios';

import Tag from './Tag';
import TextWithLabel from './TextWithLabel';

const minCharsTyped = 2, defaultMaxTags = 3;
let inputEl;

export default class TagInput extends React.Component {
    constructor(props){ 
        super(props);

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleKeyup = this.handleKeyup.bind(this); 
        
        this.timeout;
    }

    addTag(value){
        let tags = this.state.tags.slice();
        if(value.length < minCharsTyped || tags.indexOf(value)>-1) return;

        tags.push(value);
        this.setState(
            { tags: tags, value: '', suggestions: [] }, 
            () => { if (tags.length < this.maxTags) inputEl.focus(); }
        );
        
        this.handleTagsChange({target: {value: tags, name: this.props.type}});
        // send new tag to be saved
        axios.post(`/api/tag/${this.props.type}/${this.state.value}`);
    }

    componentWillMount(){
        this.setState({ tags: (this.props.tags.filter(Boolean)), value:'', suggestions: [] });
    }

    handleInputChange(event) {
        // clear timeout so suggestions are only called if user stops typing
        clearTimeout(this.timeout);
       
        // clear suggestions if the length is/goes under 2 chars
        if(event.target.value.length < minCharsTyped && this.state.suggestions.length > 0 ) 
            this.setState({ suggestions: []});
       
        // set the new value, and get suggestions after delay
        this.setState(
            { value: event.target.value }, 
            () => this.timeout = setTimeout(() => this.suggest(), 500)
        );
    }

    handleTagsChange(event) {
        if(this.props.onChange){
            if(event.target.value.length > 0){
                let val = event.target.value;
                // when sending the change to the parent if only 1 tag allowed, send as string, not array
                val = (this.props.maxTags) === 1 ? 
                    val[0] : event.target.value;
                // use target to be consistent with regular events
                event.target.value = val;
            }
            
            this.props.onChange(event);
        }
    }

    handleKeyup(event){
        // enter
        if(event.keyCode === 13)
           this.addTag(event.target.value);
    }

    removeTag(idx){
        const tags = this.state.tags.slice();
        tags.splice(idx, 1);
        // set tags after remove
        this.setState({ tags: tags }, () => {inputEl.focus();});
        this.handleTagsChange({ target: { value: tags, name: this.props.type } });
    }

    render(){
        // construct existing tags
        const tagEls = this.state.tags.map((t, i) => (
           <Tag key={i} tag={t} remove={this.removeTag.bind(this, i)} />
        ));

        // construct suggestions
        const suggestEls = this.state.suggestions.map((s, i)=> (
            <li class="tag-suggestion" onClick={this.addTag.bind(this, s.text)} key={i}>
                {s.text}
            </li>
        ));

        return <div className={(this.props.cls||"") + " tag-input-container"}>
            <label>{this.props.label}</label>
            
            <div class="tags-list-container">
                {tagEls.length < this.props.maxTags && <TextWithLabel 
                    refEl={input => inputEl = input}
                    error={this.props.error} 
                    type="text" 
                    value={this.state.value} 
                    onChange={this.handleInputChange} 
                    onKeyUp={this.handleKeyup} 
                    maxLength={this.props.maxLength || "20"}
                    noGroup={true}
                    // dont want to blur before click
                    onBlur={() =>
                        setTimeout(() => { 
                            if(this.state.suggestions.length > 0) this.setState({ suggestions:[] });
                        }, 300)
                    }
                />}

                {tagEls}
            </div>
            {suggestEls.length > 0 && <ul class="tag-suggestions-container">{suggestEls}</ul>}
        </div>
    }

    suggest(){
        if(this.state.value.length < 2) return;
        
        const url = `/api/tag/${this.props.type}/${this.state.value}`;
        axios.get(url).then(resp => this.setState({ suggestions: resp.data }));
    }
};