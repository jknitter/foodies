import React from 'react';
import classnames from 'classnames';

export default class TextWithLabel extends React.Component {
	constructor(props){
		super(props)
	}
	
	render(){
		const { refEl, cls='', noGroup, ...props } = this.props;
		const input = <input ref={input => refEl ? refEl(input): null} {...props} />;
		const textarea = <textarea {...props} />;
		const el = props.type === "area" ? textarea : input;
		const useFormGroup = !noGroup;

		const {label, error} = props;	
		return (
			<div className={classnames(cls, {'form-group': useFormGroup, invalid: error})}>
				{ label && <label for={name}>{ label }</label> }
				{el}
				{ error && <span className="help-block">*{error}</span> }
			</div>
		)
	}
}