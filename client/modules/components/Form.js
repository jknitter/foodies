import React from 'react';
import axios from 'axios';

// takes in a form, save and close
export default class ChangePasswordForm extends React.Component {
    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        const { dispatch, close, ...rest } = this.props;
        this.setState({ location: rest, errors: {} });
    }

    isValid() {
        const { errors, isValid } = validateChangePwd(this.state.location);
        if (!isValid)
            this.setState({ errors });

        return isValid;
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        const { errors } = this.state;

        return <div class="form-container">
           {this.props.form}

            <ul class="overlay-footer">
                <li class="active" onClick={this.props.close}>
                    <i class="fa fa-trash" />Cancel
                </li>
                <li class="active" onClick={this.props.save}>
                    <i class="fa fa-save" />Save
                </li>
            </ul>
        </div>
    }
};