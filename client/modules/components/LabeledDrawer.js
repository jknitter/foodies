import React from 'react';

export default class LabeledDrawer extends React.Component {
	constructor(props){
		super(props);
	}

	componentWillMount(){
		this.setState({ open: false });
	}

	render(){
		const cls = 'drawer' + (this.state.open ? ' open': '');
		const editBtn = (
			<li class='icon-w-text' onClick={this.props.edit}>
				<i class="fa fa-pencil" />
			</li>
		);

		const rmvBtn = (
			<li class='icon-w-text' onClick={this.props.remove}>
				<i class="fa fa-trash-o" />
			</li>
		);

		return <div className= "labled-drawer">
			<div class="label" onClick={() => this.setState({ open: !this.state.open })}>
				<div class="drawer-label">{this.props.label}</div>
				<ul>
					{this.state.open && this.props.edit && editBtn}	
					{this.state.open && this.props.remove && rmvBtn}	
				</ul>
			</div>
			<div className={cls}>
				{ this.props.component && <this.props.component {...this.props} /> }
			</div>
		</div>
	}
};
