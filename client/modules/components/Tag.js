import React from 'react';

export default (props) => {
    return <div class="tag">
        <span>{props.tag}</span>
        { props.remove && <span class="x" onClick={props.remove} >x</span> }
    </div>
}