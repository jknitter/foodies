import React from 'react';
import axios from 'axios';

import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/main.min.css';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

/**
 * props - 
 *  array - 
 *      picList - {String[]} list of filenames  
 *  fn - 
 *      picsRemoved
 *      picSelected
 */
export default class Pics extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const picList = ( this.props.picList || []).map((img, i) => {
            return <img key={i} src={img} />
        });

        let shownPicIndex = 0;
        const carousel = <Carousel
            onChange={(i, el) => { shownPicIndex = i }}
            infiniteLoop={true}
            showThumbs={false}
            width='100%'
        >
            {picList}
        </Carousel> 

        return <div class="pics-container">
            <div class="course-pics">
                { !picList.length && <i class="fa fa-image" /> }
                { picList.length && carousel}
            </div>
            <ul class="course-pics-buttons">
                { this.props.picsSelected && 
                    <li class="icon-w-text" onClick={() => {
                        document.querySelector('input[type=file]').click();
                        return false;
                    }}>          
                        <input type="file" name="picsUpload" accept="image/*" multiple style={{"display": "none"}} onChange={this.props.picsSelected} />
                        <i class="fa fa-cloud-upload" />
                    </li> 
                }
                {this.props.picsRemoved && <li onClick={() => { this.props.picsRemoved( shownPicIndex ) }} class="icon-w-text">
                    <i class="fa fa-trash" />
                </li> 
                }
            </ul> 
        </div>
    }
}