import React from "react"	
import { Link } from 'react-router-dom'

export default () => (
	<footer class="flex-column">
		<div class="copy">&copy; Copyright Silvarian Systems {new Date().getFullYear()}</div>
	</footer>
)