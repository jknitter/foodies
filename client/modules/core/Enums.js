import React from 'react';

const HotMeals = {
	clear: 'HOTMEALS_CLEAR',
	fetched: 'HOTMEALS_FETCHED',
	selected: 'HOTMEAL_SELECTED'
}

const Header = {
	menuClick: 'HEADER_MENU_CLICK'
}

const User = {
	event: {
		login: 'USER_LOGIN_SUCCESS',
		logout: 'USER_LOGOUT_SUCCESS',
		locationFetched: 'USER_LOCATION_FETCHED'
	},
	state: {
		info: 'USER_STATE_INFO',
		meals: 'USER_STATE_MEALS'
	}
}

const Me = {
	courseAdded: 'ME_COURSE_ADDED',
	courseEdited: 'ME_COURSE_EDITED',
	courseRemoved: 'ME_COURSE_REMOVED',
	coursesFetched: 'ME_COURSES_FETCHED',
	
	mealAdded: 'ME_MEAL_ADDED',
	mealRemoved: 'ME_MEAL_REMOVED',
	mealsFetched: 'ME_MEALS_FETCHED'
}

module.exports = { Header, HotMeals, Me, User }