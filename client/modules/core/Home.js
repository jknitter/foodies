import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import history from './history';

import Pics from '../components/Pics';
import Overlay from '../components/Overlay';

import AuthRoute from'../auth/AuthRoute';
// right panel components
import SearchPanel from '../search/SearchPanel';
import MealPanel from '../meals/MealPanel';
import MePanel from '../me/MePanel';

// home content components
import Signup from '../auth/Signup';
import Login from'../auth/Login';
import Me from '../me/Me';
import Meal from '../meals/Meal';
import SearchPage from '../search/SearchPage';

import { headerMenuClick } from '../actions/header';
import Enums from './Enums';

let unlisten;

@connect(store => {
	return { 
		showMenu: store.header.showMenu,
		meal: store.meals.meal,
		user: store.user.user,
		isAuth: store.user.isAuth
	};
})
export default class Home extends React.Component { 
	constructor(props){
		super(props);
	}

	componentWillMount(){
		this.setState({ loc: history.location.pathname.split('/')[1] });
		// Listen for changes to the current location.
		unlisten = history.listen((location, action) => {
			this.setState({ loc: location.pathname.split('/')[1] });
		});
	}

	componentWillUnmount(){
		if(unlisten) unlisten();
	}

	render(){
		const carImgs = ['food2', 'food1', 'corn', 'plating', 'praying', 'spoonful'].map((img, i) => {
			return  '/images/' + img + '.jpg';
		});

	    return <div class='content'>
			<div class="left-panel">
				<Pics picList={carImgs} />
				
				<div class="home-content">
					<Switch>
						<Route exact path="/" component={() => history.replace('/me')} />
						<AuthRoute auth={this.props.isAuth} path="/me" component={Me} />
						<Route exact path="/login" component={() => <Login {...this.props} />} />
						<Route exact path="/signup" component={() => <Signup dispatch={this.props.dispatch} />} />
						
						<Route exact path="/meal/:mealId" component={(props) => <Meal meal={this.props.meal} {...props}/> } />
						<Route exact path="/course/:mealId" component={(props) => <Meal meal={this.props.meal} {...props} />} />
						
						<Route path="/search" component={() => <SearchPage />} />
						<Route component={() => <div class="text-size-larger">404 Error</div>} />
					</Switch>
				</div>
			</div>
			
			{this.props.showMenu && this.props.isAuth && this.state.loc != 'signup' && this.state.loc != 'login' &&
				<Overlay closeOnSelect={true} close={() => { this.props.dispatch(headerMenuClick()) }} hideClose={true} cls="right-panel" align="right">
					<Switch>
						<Route path="/meal" component={MealPanel} />
						<Route path="/search" component={SearchPanel} />
						<AuthRoute auth={this.props.isAuth} path="/me" component={() => <MePanel dispatch={this.props.dispatch} />} />
					</Switch>
				</Overlay>}
	    	</div>
	}
}
