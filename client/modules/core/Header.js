import React from 'react';
import { connect } from 'react-redux';
import history from './history';
import classnames from 'classnames';
import { headerMenuClick } from '../actions/header';
import { logout } from '../actions/user';

let unlisten;
@connect(store => {
	return { 
		isAuth: store.user.isAuth,
		user: store.user.user
	 };
})
export default class Header extends React.Component {
	constructor(props){
		super(props);
	}

	componentWillMount(){
		this.setState({ menuOpen: false, loc: history.location.pathname.split('/')[1] });

		// Listen for changes to the current location.
		unlisten = history.listen((location, action) => {
			this.setState({ loc: location.pathname.split('/')[1] });
		});
	}

	componentWillUnmount(){
		if(unlisten) unlisten();
	}
	
	getNavButtonClassName(name){
		let str = '';
		// ignore the first, path always starts with a slash
		if(this.state.loc === name) str += 'active ';
		else if(name === 'menu'){
			if(!this.state.menuOpen) str += 'inactive ';
			else str += 'active ';
		}
		
		return str;
	}

	flipRightPanelDisplay(){
		// let hamburgerEl = document.querySelector('header .logo .fa-bars')[0];
		let rightPanelEl = document.getElementsByClassName('right-panel')[0];
		// // is open before flip?
		const open = this.state.menuOpen;
		const flipped = !open;

		if(flipped)
			rightPanelEl.classList.remove('inactive');
		else
			rightPanelEl.classList.add('inactive');
		// // flip in state
		this.setState({menuOpen: flipped});
	}

	render(){
		const authBtns = [
			<li key='0' className={this.getNavButtonClassName('login')} onClick={()=> history.push('/login')}>
				<i class="fa fa-sign-in" />Login
			</li>,
			<li key='1' className={this.getNavButtonClassName('signup')} onClick={()=> history.push('/signup')}>
				<i class="fa fa-address-card-o" />Signup
			</li>
		];

		const userBtns = [
			<li key='0' className={this.getNavButtonClassName('me')} onClick={()=>history.push('/me/profile')}>
				<i class="fa fa-user-o" />{this.props.user.name}
			</li>,
			<li key='1' onClick={this.signout.bind(this)}>
				<i class="fa fa-sign-out" />Logout
			</li>
		];

		return <header>
			<div class="logo">
				Meal<span>Me</span>.com
			</div>
			<nav>
				<ul>
					<li className={this.getNavButtonClassName('search')} onClick={()=>{history.push('/search/localnow')}} >
						<i class="fa fa-binoculars" />Search
					</li>						
					{ this.props.isAuth ? userBtns : authBtns }
					{
						this.props.isAuth && this.state.loc != 'signup' && this.state.loc != 'login' &&
						<li className={this.getNavButtonClassName('menu')} onClick={() => this.props.dispatch(headerMenuClick())}>
							<i class="fa fa-bars" />Menu
						</li>
					}
				</ul>
			</nav>
		</header>
	}

	signout(){
		this.props.dispatch(logout());
		history.push('/login');
	}
}