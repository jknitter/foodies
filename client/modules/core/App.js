import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import axios from 'axios';
import history from './history';

import Home from './Home';
import Header from './Header';
import Footer from './Footer';

import store from '../store';

import { processToken, logout } from '../actions/user';

class App extends React.Component {
	constructor(props){
		super(props);
	}

	render(){
		return <div class="flex-column app-content">
			<Header />
			<Switch>
				<Route path='/' component={Home} />
			</Switch>
			<Footer/>
		</div>
	}
}

// set up handler for 401, 403
axios.interceptors.response.use(
	response => response,
	err => {
		if(err.response.status === 401 || err.response.status === 403)
			history.push('/login');
		
		return;
	}
);

const token = localStorage.jwtToken;
if(token){
	store.dispatch( processToken(token) );

	if(history.location.pathname === '/' || 
	   history.location.pathname === '/login' ||
	   history.location.pathname === '/signup') 
	{
	   history.push('/me/profile');
	}   
}
else if(history.location.pathname === '/') 
	history.push('/login')

render(
	<Provider store={store}>
		<Router history={history}>
			<App />
		</Router>
	</Provider>,
	document.getElementById('app') 
);