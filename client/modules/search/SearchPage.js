import React from 'react';
import {Route} from 'react-router-dom';

import LocalNow from './LocalNow';

export default (props) => (
    <Route exact path="/search/localnow" component={() => <LocalNow {...props} />} />
)
