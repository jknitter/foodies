import React from 'react';
import history from '../core/history';

let unlisten;

export default class SearchPanel extends React.Component {
    componentWillMount(){
        this.setState({ loc: history.location.pathname.split('/')[2] });

        // Listen for changes to the current location.
        unlisten = history.listen((location, action) => {
            this.setState({ loc: location.pathname.split('/')[2] });
        });
    }

    componentWillUnmount(){
        if(unlisten) unlisten();
    }

    getNavButtonClassName(name){
        let str = '';
        // ignore the first, path always starts with a slash
        if (this.state.loc === name) str += 'active ';
        return str;
    }
    
    render(){
        return <div>
            <ul>
                <li className={this.getNavButtonClassName('localnow')} onClick={() => history.push('/search/localnow')}>
                    <i class="fa fa-flash" />Local Now
                    </li>
                <li className={this.getNavButtonClassName('advanced')} onClick={() => history.push('/search/advanced')}>
                    <i class="fa fa-search" />Advanced Search
                </li>
            </ul>
        </div>
    }
}