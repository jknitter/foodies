import axios from 'axios';
import React from 'react';
import {connect} from 'react-redux';
import history from '../core/history';

import TextWithLabel from '../components/TextWithLabel';
import { validateCourse } from '../../../shared/validate';

import { hotMealsFetched, clearMeals } from '../actions/meals';
import MealList from '../meals/MealList';

import Enums from '../core/Enums';
import utils from '../../../shared/location.util';

function jsonToQuery(obj){
	const keys = ['zip', 'distance', 'keywords'];
	let str = '';
	keys.forEach(key => { 
		if(obj[key]){
			if(str) str += '&';
			str += key + '=' + obj[key];
		}
	});

    return str;
};

function queryToJson(str) {
    return JSON.parse('{"' + decodeURI(str.replace(/&/g, "\",\"").replace(/=/g, "\":\"")) + '"}');
};

@connect(store => {
	return { 
		user: store.user.user,
		meal: store.meals.meal
	}
})
export default class LocalNow extends React.Component {
	constructor(props){
		super(props);
    }

	componentWillMount(){
        let { location } = this.props.user;
        let newState = { errors:{},  distance: 10, zip: "", coords: {}, meals: {} };
        this.setState(newState);
		
        // use query in url over other options
        const query = history.location.search;
        if(query){
			const queryJson = queryToJson(query.substring(1));
			// if zip in query 
			if(queryJson.zip){
				// can't use user's coords, get new zip's coords
				Object.assign(newState, queryJson);

				// check for user's stored location info to get at the coords
				if(location.zipcode){
					// if querying for diff zip
					if(queryJson.zip !== location.zipcode){
						// use zip to fetch coords
						utils.queryLocationByZip(queryJson.zip)
							.then(resp => {
								this.setState(Object.assign(newState, { coords: resp.data.coords }));

								// fetch data
								this.doSearch(query);
							});
					}
					if(queryJson.zip === location.zipcode){
						if(location.coords){
							// use coords
							this.setState(Object.assign(newState, { coords: location.coords }));
						
							// fetch data
							this.doSearch(query);
						}
						else {
							// use zip to fetch coords
							utils.queryLocationByZip(location.zipcode)
								.then(resp => {
									this.setState(Object.assign(newState, { coords: resp.data.coords }));

									// fetch data
									this.doSearch(query);
								});
						}
					}
				}
				else {
					if(location.coords){
						// use coords
						this.setState(Object.assign(newState, { coords: location.coords }));
						return;
						// fetch data
						this.doSearch(query);
					}
					else {
						// use zip to fetch coords
						utils.queryLocationByZip(queryJson.zip)
							.then(resp => {
								this.setState(Object.assign(newState, { coords: resp.data.coords }));

								// fetch data
								this.doSearch(query);
							});
					}
				}
			}			
        }
		// just /search in route - fill in with user data and do search
		// if logged in user has saved or previously retrieved location data
        // set the coords to be used in distance and the zip for display in txt
        else if(location){
            if(location.zipcode){
				this.setQuery(jsonToQuery({ zip: location.zipcode, distance: newState.distance }) );
			}
			// not sure how this would happen, but..
			else if(!location.zipcode && location.coords){
				// use coords to fetch zip
				utils.queryLocationByCoords(coords.lat, coords.lon)
					.then(resp => this.setQuery(jsonToQuery({ zip: resp.data.zipcode, distance: newState.distance })));
            }
        }
    }

    doSearch(query){
        return axios.get('/api/search/'+ query)
            .then(json => {
                // reset after search
                this.setState({ ...this.state, meals: json.data });
                this.props.dispatch(hotMealsFetched(json.data));
            })
            .catch(resp => { 
				const err = resp.response.data.errors;
				this.setState({ errors: err });
			});
    }

	locInputChange(e){
		const val = e.target.value;
		if(val.match(/^\d{0,5}$/)){ 
			return this.setState({zip: val});
		}
	}

	onChange(e) {
		this.setState({ ...this.state, [e.target.name]: e.target.value });
	}

	onUseLocationClick(){
		utils.getGeoLocation( (err, coords) => { 
			if(err) return console.log('getGeoLocation.callback.err', err);
	
				return utils.queryLocationByCoords(coords.lat, coords.lon)
						.then(resp => {
							const location = resp.data;
							this.setState({ coords: { lat: coords.lat, lon: coords.lon }, zip: location.zipcode });
							// set store values
							this.props.dispatch({ type: Enums.User.event.locationFetched, payload:location });	    			
							// do search by zipcode, sorta
							this.setQuery( jsonToQuery(this.state) );
						});
			}
		);
	}

	render(){
		const { errors } = this.state;
		const noMeals = <div>Nothing to show...</div>;

		return <div class="search-page">
			<div class="top">
				<div class="title">Local Now</div>
			</div>
			<div>Find what's cooking near you.</div>
			
			<form class="search-form">
				<label class="zip-label">Zipcode</label>
				<div class="zip">
					<TextWithLabel
						error={errors.zip}
						name='zip'
						onChange={this.locInputChange.bind(this)} 
						value={this.state.zip}
						maxLength="5"
						autoFocus
					/>
				</div>

				<label class="dist-label">Distance</label>
				<TextWithLabel
					error={errors.distance}
					name='distance'
					onChange={this.onChange.bind(this)}
					value={this.state.distance}
					type="number"
					min="0" max="30" step="5" 
					cls="dist"
				/>

				<ul class="loc">
					<li class="icon-w-text" onClick={this.onUseLocationClick.bind(this)} >
						<i title="Location" class="fa fa-map-marker"></i>
						<span>Get Location</span>
					</li>
				</ul>
				<ul class="search-btn">
					<li class="icon-w-text" onClick={() => this.setQuery(jsonToQuery(this.state))}>
						<i title="Search" class="fa fa-search"></i>
						<span>Find Food</span>
					</li>
				</ul>
			</form>
			
			{ Object.keys(this.state.meals).length == 0 && noMeals }
			{ Object.keys(this.state.meals).length > 0 && <MealList 
				selected={this.props.meal}
				loc={{coords: this.state.coords, zip: this.state.zip}}
				dispatch={this.props.dispatch} 
				meals={this.state.meals} 
			/>}
		</div>
    }
    
    setQuery(query) {
        if(history.location.search !== "?" + query)
			history.replace('/search/localnow?' + query);
		else
			this.doSearch("?"+query);
    }
}