
const dataJS = require('../files');
const excel = require('./excel.ctlr');
const fs = require('fs');

let repo = {
	users: dataJS.users,
	hotmeals: dataJS.hotmeals
};

const fEnums = {
	zipcoords: {
		in: 'data/files/zipcoords.xlsx',
		id: 'zipcode'
		//, out: './data/files/zipcoords.json'
	}
}

const getData = (file, idToReturn) => {
	if(!file || !repo[file]) return;
	if(!idToReturn){ 
		return repo[file];
	}
	return repo[file][idToReturn];
};

const transformColAsId = colKey => {
	return (rows) => {
		let json = {};
		if(colKey){
			// insert row into json using the value at colKey as 
			// the property name of the new json object
			rows.forEach(row => {
				let newRow = Object.assign({}, row);
				newRow.coords = {lat: parseFloat(newRow.lat), lon: parseFloat(newRow.lon)};
				delete newRow.lat;
				delete newRow.lon;

				json[ newRow[colKey] ] = newRow;
			});
		}
		else
			json = rows;

		return json;
	}
};


// load data/files for operations
Object.keys(fEnums).forEach(key => {
	file = fEnums[key];
	repo[key] = excel.readExcel(file.in, transformColAsId(file.id));
});


// EXPORTS //
module.exports.getData = getData;