const xlsx = require('xlsx');
const path = require('path');
const fs = require('fs');

const readExcel = (url, transform, out=false) => {
	const wb = xlsx.readFile(  path.resolve( url ) );
	const ws = wb.Sheets[ wb.SheetNames[0] ];
	let json = xlsx.utils.sheet_to_json(ws);

	if(transform && typeof(transform) === 'function')
		json = transform(json);

	if(out)
		fs.writeFile(path.resolve(out), JSON.stringify(json, null, 3), {flag:'w+'});

	return json;
}

module.exports.readExcel = readExcel;