const cuisines = require('./cuisines');
const users = require('./users');
const hotmeals = require('./hotmeals');
const lookup = require('./lookups');

module.exports.cuisines = cuisines;
module.exports.users = users;
module.exports.hotmeals = hotmeals;
module.exports.lookup = lookup;