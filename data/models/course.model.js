'use strict';

const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

const CourseSchema = new Schema({
	// foreign key to Users > userId
	cookId: { type: Schema.ObjectId, ref: 'User' },
	created: { type: Date, default: Date.now },
	description: String,
	// foreign keys from Courses
	ingredients: [{ type: String }],
	name: String,
	nutrition: {},
	pics: [String],
	type: String, // dinner, desert, 
	tags: [String],
});

mongoose.model('Course', CourseSchema, 'Courses');