'use strict';

const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

const TagSchema = new Schema({
    text: String,
    type: String
});

// compound unique index
TagSchema.index({text: 1, type: 1}, {unique: true});

mongoose.model('Tag', TagSchema, 'Tags');