'use strict';

const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

const MealSchema = new Schema({
	created: { type: Date, default: Date.now },
	name: String,
	description: String,
	// foregin key to Users > userId
	cookId: { type: Schema.ObjectId, ref: 'User' },
	// froegin keys from Courses
	courses: [{ type: Schema.ObjectId, ref: 'Course' }],
	price: Number,
	time: Date
});

mongoose.model('Meal', MealSchema, 'Meals');