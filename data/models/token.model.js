'use strict';

const crypto = require('crypto'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema;

const TokenSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	userId: { type: Schema.ObjectId, ref: 'User'},
	expires:{ type: Date },
  	token:{ type: String },
	type:{ type: String }
});

// Hook a pre save method to hash the password
TokenSchema.pre('save', function(next){
	var expiresDate = Date.now() + (3600000 * 24) * 3;
	this.expires = new Date(expiresDate);
	
	// create token
	var buffer = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
	this.token = buffer.toString('hex');

	next();
});

mongoose.model('Token', TokenSchema, 'Tokens');