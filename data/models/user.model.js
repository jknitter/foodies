'use strict';

const crypto = require('../../shared/crypto'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// function validateLocalStrategyProperty(property) {
// 	return ((this.provider !== 'local' && !this.updated) || property.length);
// };

// function validateLocalStrategyPassword(password) {
	// 	return (this.provider !== 'local' || (password && password.length > 7));
// };

const UserSchema = new Schema({
	avatar: { type: Boolean, default: false },
	courses: [{ type: Schema.ObjectId, ref: 'Course' }],
	created: { type: Date, default: Date.now },
	email: {
		type: String,
		trim: true,
		default: '',
		//		validate: [validateLocalStrategyProperty, 'Invalid email address'],
		//		match: [/.+\@.+\..+/, 'Invalid email address']
	},
	location: {
		address: { type: String, default: ''},
		coords: { lat: Number, lon: Number },
		country: String,
		county: String,
		placename: String,
		state: String,
		statename: String,
		zipcode: String
	},
	meals: [{ type: Schema.ObjectId, ref:'Meal' }],
	name: String,
	password: {
		type: String,
		default: ''
	},
	phone: String,
	role: {
		type: String,
		enum: ['pending', 'user', 'admin'],
		default: ['pending']
	},
	salt: String,
	userId: {
		type: String,
		unique: "User already exists",
		required: 'Please fill in a user name',
		trim: true
	}
});

// Hook a pre save method to hash the password
UserSchema.pre('save', function(next) {
	if (this.password)
		this.password = this.hashPassword(this.password);

	next();
});

//Create instance method for hashing a password
UserSchema.methods.hashPassword = function(password){ 
	return crypto.encrypt(password); 
};

// Create instance method for authenticating user
UserSchema.methods.authenticate = function(password){ 
	return this.password === this.hashPassword(password); 
};

// Find possible not used userName
// UserSchema.statics.findUniqueUsername = (userName, suffix, callback) => {
// 	const possibleUsername = userName + (suffix || '');

// 	this.findOne({ userName: possibleUsername }, (err, user) => {
// 		if(!err){
// 			if (!user) callback(possibleUsername);
// 			else return this.findUniqueUsername(userName, (suffix || 0) + 1, callback);
// 		} 
// 		else callback(null, user);
// 	});
// };

 mongoose.model('User', UserSchema, 'Users');