'use strict';

const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

const IngredientSchema = new Schema({
	name: String,
	group: String,
	type: String,
	tags: {}, 
	organic: Boolean,
	nutrition: {}
});

mongoose.model('Ingredient', IngredientSchema, 'Ingredients');