const Validator = require('validator');

const LoN = "Only Letters or Numbers";
const NO = 'Number Only'; 
const LO = 'Letters Only'; 
const required = "Required";
const pwdLen = "Must be 8 to 15 characters";
const nameLen = "Must be 4 to 10 characters";

	// regex: {
	// 	zip: /^\d{5}(?:[-\s]\d{4})?$/
	// },

module.exports.validateCourse = function(data){
	let errors = {};

	if(Validator.isEmpty(data.name)){
		errors.name = required;
	}

	// if(!Validator.isNumeric(data.portionSize.toString())){
	// 	errors.portionSize = NO;
	// }

	// if(!data.portionUnits){
	// 	errors.portionUnits = required;
	// }
	// else if(!Validator.isAlpha(data.portionUnits)){
	// 	errors.portionUnits = NO;
	// }

	return {
		errors: errors,
		isValid: Object.keys(errors).length === 0
	}
}


module.exports.validateMeal = function (data) {
	let errors = {};

	return {
		errors: errors,
		isValid: Object.keys(errors).length === 0
	}
}

////////////////// PROFILE ///////////////////
module.exports.validateProfileLocation = function(data){
	let errors = {}; 
	// // letters, numbers, period; no less than 4 chars
	// if(!data.address.match(/^[\w\s\.]{4,}$/)){
	// 	errors.address = "Invalid Address";
	// }

	return {
		errors: errors,
		isValid: Object.keys(errors).length === 0
	}
}

//////////////// End PROFILE ////////////////

/////////////////// AUTH ////////////////////
module.exports.validateSignup = function(data){
	let errors = {};
	// UserId
	if(Validator.isEmpty(data.userId)){
		errors.userId = required;
	}
	else if(!Validator.isAlphanumeric(data.userId)){
		errors.userId = LoN;
	}
	else if(data.userId.length < 4 || data.userId.length > 10){
		errors.userId = nameLen;
	}
	
	// name
	if(Validator.isEmpty(data.name)){
		errors.name = required;
	}
	else if(!Validator.isAlphanumeric(data.name)){
		errors.name = LoN;
	}
	else if(data.name.length < 4 || data.name.length > 10){
		errors.name = nameLen;
	}

	// Email
	if(Validator.isEmpty(data.email)){
		errors.email = required;
	}
	else if(!Validator.isEmail(data.email)){
		errors.email = "Invalid email";
	}

	// Zipcode
	if(Validator.isEmpty(data.zip)){
		errors.zip = required;
	}
	else if(!data.zip.match(/^\d{5}$/)){
		errors.zip = "Invalid Zipcode";
	}
	
	// password
	if(Validator.isEmpty(data.pwd1)){
		errors.pwd1 = required;
	}
	else if(!Validator.isAlphanumeric(data.pwd1)){
		errors.pwd1 = LoN;
	}
	else if(data.pwd1.length < 8 || data.pwd1.length > 15){
		errors.pwd1 = pwdLen;
	}

	// confirm password
	if(Validator.isEmpty(data.pwd2)){
		errors.pwd2 = required;
	}
	else if(!Validator.isAlphanumeric(data.pwd2)){
		errors.pwd2 = LoN;
	}
	else if(data.pwd2.length < 8 || data.pwd2.length > 15){
		errors.pwd2 = pwdLen;
	}
	else if(!Validator.equals(data.pwd1, data.pwd2)){
		errors.pwd2 = "Passwords Don't Match";
	}

	return {
		errors: errors,
		isValid: Object.keys(errors).length === 0
	}
}

module.exports.validateLogin = function(data){
	let errors = {};
	const LoN = "Only Letters or Numbers";
	const required = "Required";
	const pwdLen = "Must be 8 to 15 characters";
	const nameLen = "Must be 4 to 10 characters";

	// UserId
	if(Validator.isEmpty(data.userId)){
		errors.userId = required;
	}
	else if(!Validator.isAlphanumeric(data.userId)){
		errors.userId = LoN;
	}
	else if(data.userId.length < 4 || data.userId.length > 10){
		errors.userId = nameLen;
	}

	// password
	if(Validator.isEmpty(data.pwd1)){
		errors.pwd1 = required;
	}
	else if(!Validator.isAlphanumeric(data.pwd1)){
		errors.pwd1 = LoN;
	}
	else if(data.pwd1.length < 8 || data.pwd1.length > 15){
		errors.pwd1 = pwdLen;
	}

	return {
		errors: errors,
		isValid: Object.keys(errors).length === 0
	}
}
///////////////// END AUTH /////////////////

