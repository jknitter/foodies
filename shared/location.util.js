const axios = require('axios');

'use strict';

const Rm = 3961; // mean radius of the earth (miles) at 39 degrees from the equator
const Rk = 6373; // mean radius of the earth (km) at 39 degrees from the equator

// convert degrees to radians
function deg2rad(deg) {
	return deg * Math.PI/180; // radians = degrees * pi/180
}

module.exports = {
	getDistanceFromLatLons: function(lat1, lon1, lat2, lon2, unit){
		const R = 6378137; // Earth’s radius in meter
		const dLat = deg2rad(lat2 - lat1);
		const dLong = deg2rad(lon2 - lon1);
		const a = Math.sin(dLat / 2) * 
				Math.sin(dLat / 2) + 
				Math.cos(deg2rad(lat1)) * 
				Math.cos(deg2rad(lat2)) * 
				Math.sin(dLong / 2) * 
				Math.sin(dLong / 2);

	  	const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	  	let d = R * c;
	  	// kilometers or miles
	  	d = unit === 'K' ? d : d * .62;
	  	// adjust for crow flies, traffic
	  	d = d * 1.40; 
	  	// round
	  	d = Math.trunc( d /100 ) / 10;
	  	return d;
	},

	// browser only
	getGeoLocation: function(callback){
		if(navigator && navigator.geolocation){
	        navigator.geolocation.getCurrentPosition(
	        	position => { 
	        		// we want to ba able to use lat, lon
	        		const { latitude, longitude } = position.coords;
	        		callback(null, {lat: latitude, lon: longitude});
	        	},
				err => { 
					console.log('getLocation.err', err); 
					callback(err);
				},
				{maximumAge: 0, timeout:5000, enableHighAccuracy: true}
			);
		}
		else {
			console.log('getGeoLocation.err', 'No navigator');
		}
	},

	queryLocationByCoords(lat, lon) {
		return axios.get(`/api/location/${lat}/${lon}`)
	},
	
	queryLocationByZip(zip) {
		return axios.get(`/api/location/${zip}`)
	}
}